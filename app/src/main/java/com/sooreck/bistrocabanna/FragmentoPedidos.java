package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 03/03/2016.
 */
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Fragmento para la pestaña "Pedidos" pala la sección de "Mi Cuenta"
 */

public class FragmentoPedidos extends Fragment {


    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String LOGIN_URL = "http://www.mariocastro.96.lt/basedatos/selectpedidos.php";

    private SwipeRefreshLayout refreshLayout;

    // La respuesta del JSON del vector contiene si se completo, un mensaje y los datos del cliente
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_PEDIDOS = "pedidos";

    private JSONArray datosPedidos = null;
    // TAGS para recuperar informacion del cliente;
    private static final String TAG_IDORDER = "idorder";
    private static final String TAG_USER = "user";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_PLATILLO = "platillo";
    private static final String TAG_INGREDIENTES = "ingredientes";
    private static final String TAG_PRECIO = "preciototal";
    private static final String TAG_FECHA = "fechahora";
    private static final String TAG_ESTADO = "status";


    private LinearLayoutManager linearLayout;
    public static RecyclerView reciclador;
    public static AdaptadorPedidos adaptador;

    public static List<Pedidos> items;
    Cliente cliente;
    public static Pedidos pedido;


    public FragmentoPedidos() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_grupo_items, container, false );

        reciclador = (RecyclerView)view.findViewById(R.id.reciclador);
        linearLayout = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayout);

        items = new ArrayList<>();
        Intent i = getActivity().getIntent();
        cliente = (Cliente)i.getSerializableExtra("cliente");

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        );


        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new TraerPedidos().execute(cliente.getUser());
                    }
                }
        );

        new TraerPedidos().execute(cliente.getUser());



        return view;
    }

    class TraerPedidos extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Buscando Pedidos...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show(); */
        }

        @Override
        protected String doInBackground(String... args) {

            int success;
            String username = args[0];

            try {
                // Building Parameters
                //List params = new ArrayList();
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user", username));
                Log.d("request!", "starting");
                // getting product details by making HTTP request
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",
                        params);
                success = 99;
                try {
                    success = json.getInt(TAG_SUCCESS);
                }

                catch(Exception e){

                    Log.d("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }

                if (success == 1) {
                    Log.d("Si tiene pedidos", json.toString());

                    ArrayList<String> list = new ArrayList<String>();
                    datosPedidos = json.getJSONArray(TAG_PEDIDOS);

                    String idorderExt = "";
                    String userExt = "";
                    String addressExt = "";
                    String platilloExt = "";
                    String ingredientesExt = "";
                    String fechaExt = "";
                    String estadoExt = "";
                    String precioExt = "";

                    items.clear();

                    if (datosPedidos != null) {

                        for (int i = 0; i < datosPedidos.length(); i++) {
                            JSONObject c = datosPedidos.getJSONObject(i);

                            idorderExt = c.getString(TAG_IDORDER);
                            list.add(idorderExt);
                            Log.e("!!!!!DATOS!!!!!", idorderExt);

                            userExt = c.getString(TAG_USER);
                            list.add(userExt);
                            Log.e("!!!!!DATOS!!!!!", userExt);

                            addressExt = c.getString(TAG_ADDRESS);
                            list.add(addressExt);
                            Log.e("!!!!!DATOS!!!!!", addressExt);

                            platilloExt = c.getString(TAG_PLATILLO);
                            list.add(platilloExt);
                            Log.e("!!!!!DATOS!!!!!", platilloExt);

                            ingredientesExt = c.getString(TAG_INGREDIENTES);
                            list.add(ingredientesExt);
                            Log.e("!!!!!DATOS!!!!!", ingredientesExt);

                            fechaExt = c.getString(TAG_FECHA);
                            list.add(fechaExt);
                            Log.e("!!!!!DATOS!!!!!", fechaExt);

                            estadoExt = c.getString(TAG_ESTADO);
                            list.add(estadoExt);
                            Log.e("!!!!!DATOS!!!!!", estadoExt);

                            precioExt = c.getString(TAG_PRECIO);
                            list.add(precioExt);
                            Log.e("!!!!!DATOS!!!!!", precioExt);

                            pedido = new Pedidos(platilloExt, addressExt, fechaExt, estadoExt,precioExt);
                            Log.e("!!!!!GUARDADO!!!!!", pedido.getPlatillo());

                            items.add(pedido);

                        }

                    }

                    adaptador = new AdaptadorPedidos(items);


                    return json.getString(TAG_MESSAGE);
                } else {
                    Log.d("ERROR", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            refreshLayout.setRefreshing(false);
            //pDialog.dismiss();
            if (file_url != null) {
                iniciarSetAdapter();
                Toast.makeText(getActivity(), file_url, Toast.LENGTH_LONG).show();
            }
        }
    }


    public static void iniciarSetAdapter(){
        reciclador.setAdapter(adaptador);
    }



}// Termina clase fragmentoPedidos