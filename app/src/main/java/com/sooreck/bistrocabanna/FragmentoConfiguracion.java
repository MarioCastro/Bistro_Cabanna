package com.sooreck.bistrocabanna;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 30/03/2016.
 */
public class FragmentoConfiguracion extends Fragment {

    private Switch notificaciones;
    private TextView info;

    private Cliente cliente;
    private ProgressDialog pDialog;
    private boolean casoBool;

    // JSON  class
    JSONParser jsonParser = new JSONParser();

    private static final String URL_INSERT = "http://mariocastro.96.lt/basedatos/insert_token.php";
    private static final String URL_DELETE = "http://mariocastro.96.lt/basedatos/delete_token.php";

    //ids
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_CASO = "caso";

    public FragmentoConfiguracion(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_configuracion, container, false );
        Intent i = getActivity().getIntent();
        final SharedPreferences sp = getActivity().getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
        info = (TextView) view.findViewById(R.id.textViewInfo);

        notificaciones = (Switch) view.findViewById(R.id.switchNotificaciones);

        cambiarestado();

        notificaciones.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferences = getActivity().getSharedPreferences("notify", Context.MODE_PRIVATE);
                String token = "";
                token = preferences.getString("token","");

                if (isChecked){
                    new InsertToken().execute(sp.getString("username",""),token,"INSERTAR");

                }
                if (!isChecked) {
                    new InsertToken().execute(sp.getString("username", ""),token,"BORRAR");
                }
            }
        });


        return view;

    }

    private void cambiarestado(){
        SharedPreferences preferences = getActivity().getSharedPreferences("notify", Context.MODE_PRIVATE);
        boolean recibir = preferences.getBoolean("recibir",false);
        notificaciones.setChecked(recibir);
        if (recibir){
            /*pasar la info a false*/
            info.setText(getResources().getString(R.string.info_notificaciones_false));
        } else {
           /*pasar la info a true*/
            info.setText(getResources().getString(R.string.info_notificaciones_true));
        }
    }

    /***********************************************************************************************
     ************************* CLASE ASINCRONA PARA INSERTAR TOKEN EN BD **************************
     * ********************************************************************************************/

    class InsertToken extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Solicitando Servicio...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {

            // Check for success tag
            int success;

            String username = args[0];
            String token =args[1];
            String caso = args[2];

            try {
                // Building Parameters
                List params = new ArrayList();
                params.add(new BasicNameValuePair("username", username));
                params.add(new BasicNameValuePair("token", token));



                Log.d("request!", "starting");

                JSONObject json = null;

                if (caso.equals("INSERTAR")){
                    json = jsonParser.makeHttpRequest(
                            URL_INSERT, "POST", params);
                }
                if (caso.equals("BORRAR")){
                    json = jsonParser.makeHttpRequest(
                            URL_DELETE, "POST", params);
                }

                // full json response
                Log.d("Registrando intento", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                casoBool = json.getBoolean(TAG_CASO);
                if (success == 1) {
                    return json.getString(TAG_MESSAGE);
                }else{
                    return json.getString(TAG_MESSAGE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            if (file_url != null){
                pDialog.dismiss();
                if (casoBool){
                    SharedPreferences preferences = getActivity().getSharedPreferences("notify", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putBoolean("recibir",true);
                    edit.apply();
                    cambiarestado();
                }
                if (!casoBool){
                    SharedPreferences preferences = getActivity().getSharedPreferences("notify", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putBoolean("recibir",false);
                    edit.apply();
                    cambiarestado();
                }


            }
        }
    }// Termina la clase para agregar pedido a la base de datos

}
