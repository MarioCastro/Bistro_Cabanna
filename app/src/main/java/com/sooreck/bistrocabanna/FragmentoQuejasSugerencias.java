package com.sooreck.bistrocabanna;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentoQuejasSugerencias extends Fragment {

    private LinearLayoutManager linearLayout;
    public static RecyclerView reciclador;
    public static AdaptadorQuejasSugerencias adaptador;
    private SwipeRefreshLayout refreshLayout;

    // La respuesta del JSON del vector contiene si se completo, un mensaje y los datos de los comentarios
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_COMENTARIOS = "comentarios";

    private static final String TAG_SEND_SUCCESS = "success";
    private static final String TAG_SEND_MESSAGE = "message";
    // Progress Dialog
    private ProgressDialog pDialog;

    // JSON  class
    JSONParser jsonParserInsertComentario = new JSONParser();


    JSONParser jsonParser = new JSONParser();

    private static final String URL = "http://www.mariocastro.96.lt/basedatos/select_quejas_sugerencias.php";
    private static final String INSERT_URL = "http://www.mariocastro.96.lt/basedatos/insert_comentario.php";

    private JSONArray datosComentarios = null;
    // TAGS para recuperar informacion del cliente;
    private static final String TAG_IDQUEJA = "idqueja";
    private static final String TAG_USER = "user";
    private static final String TAG_RATING = "rating";
    private static final String TAG_COMENTARIO = "comentario";
    private static final String TAG_FECHA = "fecha";


    private RatingBar ratingBar;
    private EditText etComentario;
    private FloatingActionButton send;

    public static List<QuejasSugerencias> items;
    Cliente cliente;
    public static QuejasSugerencias comentario;

    public FragmentoQuejasSugerencias() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_quejas_sugerencias, container, false );

        reciclador = (RecyclerView)view.findViewById(R.id.reciclador_comentarios);
        linearLayout = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayout);
        ratingBar = (RatingBar) view.findViewById(R.id.rating);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating == 0) {
                    ratingBar.setRating(1);
                } else
                    ratingBar.setRating(rating);
            }
        });

        items = new ArrayList<>();
        Intent i = getActivity().getIntent();
        cliente = (Cliente)i.getSerializableExtra("cliente");

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        );


        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new TraerComentarios().execute("");
                    }
                }
        );

        etComentario = (EditText) view.findViewById(R.id.editTextComent);
        send = (FloatingActionButton) view.findViewById(R.id.fab_send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datosCorrectos()) {
                    new EnviarComentario().execute(cliente.getUser(),etComentario.getText().toString(),String.valueOf(ratingBar.getRating()));
                }
            }
        });

        new TraerComentarios().execute("");

        return view;
    }


    class TraerComentarios extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Buscando Comentarios...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show(); */
        }

        @Override
        protected String doInBackground(String... args) {

            int success;

            try {
                // Building Parameters
                //List params = new ArrayList();
                String a = args[0];
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user", a));
                Log.d("request!", "starting");
                // getting product details by making HTTP request
                JSONObject json = jsonParser.makeHttpRequest(URL, "POST",
                        params);
                success = 99;
                try {
                    success = json.getInt(TAG_SUCCESS);
                }

                catch(Exception e){

                    Log.d("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }

                if (success == 1) {
                    Log.d("Si tiene pedidos", json.toString());


                    ArrayList<String> list = new ArrayList<String>();
                    datosComentarios = json.getJSONArray(TAG_COMENTARIOS);

                    String idquejaExt;
                    String userExt;
                    String ratingExt;
                    String comentarioExt;
                    String fechaExt;

                    items.clear();

                    if (datosComentarios != null) {

                        for (int i = 0; i < datosComentarios.length(); i++) {
                            JSONObject c = datosComentarios.getJSONObject(i);

                            idquejaExt = c.getString(TAG_IDQUEJA);
                            list.add(idquejaExt);
                            Log.e("!!!!!DATOS!!!!!", idquejaExt);

                            userExt = c.getString(TAG_USER);
                            list.add(userExt);
                            Log.e("!!!!!DATOS!!!!!", userExt);

                            ratingExt = c.getString(TAG_RATING);
                            list.add(ratingExt);
                            Log.e("!!!!!DATOS!!!!!", ratingExt);

                            comentarioExt = c.getString(TAG_COMENTARIO);
                            list.add(comentarioExt);
                            Log.e("!!!!!DATOS!!!!!", comentarioExt);

                            fechaExt = c.getString(TAG_FECHA);
                            list.add(fechaExt);
                            Log.e("!!!!!DATOS!!!!!", fechaExt);

                            comentario = new QuejasSugerencias(userExt, comentarioExt, fechaExt, Integer.parseInt(ratingExt));
                            Log.e("!!!!!GUARDADO!!!!!", String.valueOf(comentario.getRating()));

                            items.add(comentario);

                        }

                    }

                    adaptador = new AdaptadorQuejasSugerencias(items);


                    return json.getString(TAG_MESSAGE);
                } else {
                    Log.d("ERROR", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            refreshLayout.setRefreshing(false);
            //pDialog.dismiss();
            if (file_url != null) {
                iniciarSetAdapter();
                Toast.makeText(getActivity(), file_url, Toast.LENGTH_LONG).show();
            }
        }
    }


    public static void iniciarSetAdapter(){
        reciclador.setAdapter(adaptador);
    }

    class EnviarComentario extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Enviando Comentario...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            int success;
            String usuarioo = args[0];
            String comentarioo = args[1];
            String ratingg = args[2];

            try {
                ArrayList<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("username", usuarioo));
                params.add(new BasicNameValuePair("rating", ratingg));
                params.add(new BasicNameValuePair("comentario", comentarioo));

                Log.d("request!", "starting");
                JSONObject json = jsonParserInsertComentario.makeHttpRequest(INSERT_URL, "POST",
                        params);
                success = 99;
                try {
                    success = json.getInt(TAG_SEND_SUCCESS);
                }

                catch(Exception e){

                    Log.d("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }

                if (success == 1) {
                    Log.d("correcto", json.toString());

                    return json.getString(TAG_SEND_MESSAGE);
                } else {
                    Log.d("ERROR", json.getString(TAG_SEND_MESSAGE));
                    return json.getString(TAG_SEND_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            if (file_url != null) {
                new TraerComentarios().execute("");
                Toast.makeText(getActivity(), file_url, Toast.LENGTH_LONG).show();
                etComentario.setText("");
            }
        }
    }

    private boolean datosCorrectos(){

        if (etComentario.getText().toString().equals("")){
            Toast.makeText(getActivity(), "Tu comentario esta vacio", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etComentario.getText().toString().length() >= 250){
            Toast.makeText(getActivity(), "Tu comentario excede el límite de tamaño", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}// Termina la clase de FragmentoQuejasSugerencias
