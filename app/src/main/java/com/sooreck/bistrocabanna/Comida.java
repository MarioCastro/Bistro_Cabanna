package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 27/02/2016.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Clase de Comida para poblar la aplicacion.
 * Contiene promociones en el fragmento inicio
 * Contiene bebidas para el tab bebidas del FragmentoPestanaMenu
 * Contiene postres para el tab postres del fragmentoPestanaMenu
 * Contiene platillos para el tab platillos del fragmentoPestanaMenu
 */
public class Comida {
    private String precio;
    private String nombre;
    private int idDrawable;
    private int editable;

    /*Constructor para inicializar los platillos, bebidas y postres*/

    public Comida(String precio, String nombre, int idDrawable, int edit) {
        this.precio = precio;
        this.nombre = nombre;
        this.idDrawable = idDrawable;
        this.editable = edit;

    }

    /*Listas utilizables para los adaptadores*/
    public static final List<Comida> COMIDAS_POPULARES = new ArrayList<Comida>();
    public static final List<Comida> BEBIDAS = new ArrayList<>();
    public static final List<Comida> POSTRES = new ArrayList<>();
    public static final List<Comida> PLATILLOS = new ArrayList<>();

    /*Se generan las instancias para poder servir como fuente de alimentacion para los respectivos Adapter*/
    static {
        COMIDAS_POPULARES.add(new Comida("60.00", "Promoción Especial", R.drawable.hampromo,0));
        COMIDAS_POPULARES.add(new Comida("28.50", "Brochetas de pollo", R.drawable.brocheta,0));
        COMIDAS_POPULARES.add(new Comida("65", "Camarones Tismados", R.drawable.camarones,0));
        COMIDAS_POPULARES.add(new Comida("49.90", "Snack Especial", R.drawable.snackespecial,0));
        COMIDAS_POPULARES.add(new Comida("35", "Bistro Sushi", R.drawable.sushi,0));
        COMIDAS_POPULARES.add(new Comida("22", "Sandwich DeliCabanna", R.drawable.sandwich,0));
        COMIDAS_POPULARES.add(new Comida("60", "Lomo De Cerdo Austral", R.drawable.lomo_cerdo,0));
        COMIDAS_POPULARES.add(new Comida("32", "Pastel de Chocolate", R.drawable.pchocolate,0));

        PLATILLOS.add(new Comida("Platillo Preparable", "Hamburguesas", R.drawable.ham ,0));
        PLATILLOS.add(new Comida("Platillo Preparable", "Ensaladas", R.drawable.ens, 0));
        PLATILLOS.add(new Comida("28.50", "Brochetas de pollo", R.drawable.brocheta,0));
        PLATILLOS.add(new Comida("28.50", "Ensalada Cobby de Pollo", R.drawable.saladpollo,0));
        PLATILLOS.add(new Comida("65", "Camarones Tismados", R.drawable.camarones,0));
        PLATILLOS.add(new Comida("22", "Hamburguesa Griega", R.drawable.hgriega,0));
        PLATILLOS.add(new Comida("49.90", "Snack Especial", R.drawable.snackespecial,0));
        PLATILLOS.add(new Comida("48.50", "The Best Breakfast", R.drawable.thebestbreakfast,0));
        PLATILLOS.add(new Comida("16", "Burritos Omelet", R.drawable.burritosomelet,0));
        PLATILLOS.add(new Comida("25", "Rosca BC", R.drawable.rosca,0));
        PLATILLOS.add(new Comida("35", "Bistro Sushi", R.drawable.sushi,0));
        PLATILLOS.add(new Comida("22", "Sandwich DeliCabanna", R.drawable.sandwich,0));
        PLATILLOS.add(new Comida("60", "Lomo De Cerdo Austral", R.drawable.lomo_cerdo,0));
        PLATILLOS.add(new Comida("12.90", "Kid Burguer", R.drawable.kidburguer,0));
        PLATILLOS.add(new Comida("28", "Kid Breakfast", R.drawable.kidbreakfast,0));

        BEBIDAS.add(new Comida("10", "Taza de Café", R.drawable.cafe,0));
        BEBIDAS.add(new Comida("18", "Coctel Tronchatoro", R.drawable.coctel,0));
        BEBIDAS.add(new Comida("15", "Jugo Natural", R.drawable.jugo_natural,0));
        BEBIDAS.add(new Comida("18", "Coctel Jordano", R.drawable.coctel_jordano,0));
        BEBIDAS.add(new Comida("30", "Botella Vino Tinto Darius", R.drawable.vino_tinto,0));

        POSTRES.add(new Comida("20", "Postre De Vainilla", R.drawable.postre_vainilla,0));
        POSTRES.add(new Comida("24", "Pay de Moras", R.drawable.paydemoras,0));
        POSTRES.add(new Comida("32", "Pastel de Chocolate", R.drawable.pchocolate,0));
        POSTRES.add(new Comida("18", "Flan Celestial", R.drawable.flan_celestial,0));
        POSTRES.add(new Comida("10", "Cupcake Festival", R.drawable.cupcakes_festival,0));
        POSTRES.add(new Comida("20.50", "Pastel De Fresa", R.drawable.pastel_fresa,0));
        POSTRES.add(new Comida("22.50", "Muffin", R.drawable.muffin_amoroso,0));

    }


    /*Metodos para retornar los valores de los atributos privados*/
    public String getPrecio() {
        return precio;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public int getEditable() {return  editable; }
} /*Termina la clase Comida*/