package com.sooreck.bistrocabanna;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;


/**
 * Created by SoOreck on 19/02/2016.
 */

public class Login extends AppCompatActivity {

    // Progress Dialog utilizado en tarea asincrona
    private ProgressDialog pDialog;

    /*Objeto por el cual recibiremos informacion de la base de datos remota*/
    JSONParser jsonParser = new JSONParser();

    /* URL donde se encuentra el php qeu accede a la base de datos como intermediario*/
    private static final String LOGIN_URL = "http://www.mariocastro.96.lt/basedatos/login.php";

    // La respuesta del JSON del vector contiene si se completo, un mensaje y los datos del cliente
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_DATOS = "datos";

    // TAGS para recuperar informacion del cliente;
    private static final String TAG_USER = "user";
    private static final String TAG_PASSWORD = "password";
    private static final String TAG_NAME = "name";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_DATE = "date";
    private static final String TAG_PHONE = "phone";

    /* Objeto donde se guardara informacion recuperada de bd remota*/
    public Cliente cliente;

    /* Componentes utilizados en el layout inflado*/
    private TextInputLayout tilUser;
    private TextInputLayout tilPassword;

    private EditText campoUser;
    private EditText campoPassword;

    private Button login;
    private TextView register;
    private Button accionllamar;

    /*Recuperar el JSONArray del JSONObjet*/
    private JSONArray datosCliente = null;

    /*Atributos para la Animacion*/
    private ViewGroup linearLayoutDetails;
    private ImageView imageViewExpand;
    private static final int DURATION = 250;

    private boolean tieneDatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actividad_login);
        setSupportActionBar(toolbar);

        linearLayoutDetails = (ViewGroup) findViewById(R.id.linearLayoutDetails);
        imageViewExpand = (ImageView) findViewById(R.id.imageViewExpand);


        tilUser = (TextInputLayout) findViewById(R.id.til_usuario);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);

        campoUser = (EditText) findViewById(R.id.campo_usuario);
        campoPassword = (EditText) findViewById(R.id.campo_password);

        /*Eventos en los campos de texto para comprobar errores*/
        campoUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilUser.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        campoPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (tilPassword.getEditText().getText().toString().length() >= 8) {
                    tilPassword.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        login = (Button) findViewById(R.id.btn_login);

        /*Evento del boton Login*/
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* Se comprueba si teine conexion para iniciar sesion */
                if (tieneConexion(Login.this)) {
                    /* Si tiene conexion verifica si los datos son correctos*/
                    validarDatos();
                } else Snackbar.make(v, "Comprueba tu conexion a internet", Snackbar.LENGTH_SHORT)
                        .show();
                //Toast.makeText(Login.this,"Comprueba tu conexion a internet", Toast.LENGTH_SHORT).show();

            }
        });

        register = (TextView) findViewById(R.id.link_signup);

        /* Evento que inicia otra actividad para poder registrar un usuario */
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, ActividadRegistro.class);
                startActivity(i);
            }
        });

        accionllamar = (Button) findViewById(R.id.accionllamar);
        accionllamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:6751234567"));
                startActivity(intent);
            }
        });

        Intent i = getIntent();
        tieneDatos = i.getBooleanExtra("tieneDatos",false);
        SharedPreferences prefs =  getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
        if (tieneDatos){
            campoUser.setText(prefs.getString("username",""));
            campoPassword.setText(prefs.getString("password",""));

            boolean TieneConexion = i.getBooleanExtra("tieneConexion",false);
            if (!TieneConexion){
                Toast.makeText(Login.this, "Comprueba tu conexion a internet", Toast.LENGTH_SHORT).show();
            }
        }


    }// Fin del onCreate

  /*  public static void call(Activity a){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("6751028288"));
        a.startActivity(intent);
    } */

    public static boolean tieneConexion(Context context) {

        boolean connected = false;

        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Recupera todas las redes (tanto móviles como wifi)
        NetworkInfo[] redes = connec.getAllNetworkInfo();

        for (int i = 0; i < redes.length; i++) {
            // Si alguna red tiene conexión, se devuelve true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                connected = true;
            }
        }
        return connected;
    }// Fin del metodo tiene conexion

    private void validarDatos() {
        String usuario = tilUser.getEditText().getText().toString();
        String password = tilPassword.getEditText().getText().toString();

        boolean a = esUsuarioValido(usuario);
        boolean b = esPasswordValido(password);

        if (a && b) {
            /*Si los dos datos son validos se inicia la recuperacion de datos de la tarea asincrona*/
            new AttemptLogin().execute(tilUser.getEditText().getText().toString(), tilPassword.getEditText().getText().toString());

        }

    }// Fin del metodo validarDatos

    private boolean esUsuarioValido(String usuario) {
        if (usuario.equals("")) {
            tilUser.setError("Usuario Vacío");
            return false;
        } else {
            tilUser.setError(null);
        }

        return true;
    }
    private boolean esPasswordValido(String password) {
        if (password.equals("")) {

            tilPassword.setError("Password Vacía");
            return false;
        } else {

            if (password.length() < 8) {
                tilPassword.setError("Password Incorrecta");
                return false;

            }
            tilPassword.setError(null);
        }

        return true;
    }

    /*Metodo para la animacion y para mostrar detalles del cardiew*/
    public void toggleDetails(View view) {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.indicador_abajo);
            rotate(-180.0f);

        } else {
            ExpandAndCollapseViewUtil.collapse(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.indicador_arriba);
            rotate(90.0f);

        }
    }
    /* Metodo para rotar la animcaion de una imagen */
    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        imageViewExpand.startAnimation(animation);
    }

    /* Clase para tarea asincrona
    *  En esta clase se inicia sesion y se recuperan datos del JSON*/
    public class AttemptLogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            /*Antes de ejecutarse se mostrara un dialogo*/

            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Iniciando sesion...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            /*Se realiza en segundo plano mientras se muestra el dialog de iniciando sesion pDialog */

            int success; /*Nos permitira saber si el login fue correcto*/

            /*Se obtienen los argumentos enviados al ejecutar la tarea asincrona
            * args[0] para el usuairo
            * args[1] para la password */
            String username = args[0];
            String password = args[1];

            try {

                /* Se crean los parametros que se enviaran por POST */
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user", username));
                params.add(new BasicNameValuePair("password", password));

                Log.d("request!", "starting");

                // Obtiene el objeto JSON del HTTP request  *** Revisar Clase JSONParser ***
                /* Se envian por POST */
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",
                        params);

                /*Se obitene del objeto JSON el TAG_SUCCESS para saber si se puede iniciar sesion o no*/
                success = 99;
                try {
                    success = json.getInt(TAG_SUCCESS);
                }

                catch(Exception e){

                    Log.d("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }

                /*Si la base de datos dio acceso de login*/
                if (success == 1) {
                    Log.d("Login Correcto", json.toString());

                    // Se guardan en las preferencias el usuario y password para futuros inicios de sesion
                    SharedPreferences sp = getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);

                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString("username", username);
                    edit.putString("password",password);
                    edit.putBoolean("sesionGuardada",true);
                    edit.apply();

                    /*Se almacenaran los datos recibidos del jsonarray*/
                    ArrayList<String> list = new ArrayList<>();

                    /*del JSON array se obtiene el vector de datos para crear un nuevo Objeto tipo Usuario*/
                    datosCliente = json.getJSONArray(TAG_DATOS);

                    String userExt = "";
                    String passwordExt = "";
                    String nameExt = "";
                    String addressExt = "";
                    String dateExt = "";
                    String phoneExt = "";

                    if (datosCliente != null) {

                    for (int i = 0; i < datosCliente.length(); i++) {
                        /*Se obtiene cada dato del json para extrar los datos del usuario
                        * Se extraen
                        * Se añaden a una lista
                        *
                        * */

                        JSONObject c = datosCliente.getJSONObject(i);

                        userExt = c.getString(TAG_USER);
                        list.add(userExt);
                        Log.e("!!!!!DATOS!!!!!", userExt);

                        passwordExt = c.getString(TAG_PASSWORD);
                        list.add(passwordExt);
                        Log.e("!!!!!DATOS!!!!!", passwordExt);

                        nameExt = c.getString(TAG_NAME);
                        list.add(nameExt);
                        Log.e("!!!!!DATOS!!!!!", nameExt);

                        addressExt = c.getString(TAG_ADDRESS);
                        list.add(addressExt);
                        Log.e("!!!!!DATOS!!!!!", addressExt);

                        dateExt = c.getString(TAG_DATE);
                        list.add(dateExt);
                        Log.e("!!!!!DATOS!!!!!", dateExt);

                        phoneExt = c.getString(TAG_PHONE);
                        list.add(phoneExt);
                        Log.e("!!!!!DATOS!!!!!", phoneExt);

                    }

                    }

                    /* Se crea el objeto tipo cliente con los datos con el que se inicio sesion*/
                    cliente = new Cliente(userExt,passwordExt,nameExt,addressExt,dateExt,phoneExt);

                    /*Se inicia la actividad y se envia el objeto cliente*/
                    Intent i = new Intent(Login.this, ActividadUsuarioPrincipal.class);
                    i.putExtra("cliente", cliente);
                    finish();
                    startActivity(i);
                    return json.getString(TAG_MESSAGE);
                }

                /*Si la base de datos no dio acceso de login*/
                else {
                    Log.d("Verifica tus datos", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            /* Despues de terminar el backgroud se cierra el pDialog y se muestra un Toast si el login fue correcto o no*/
            pDialog.dismiss();
            if (file_url != null) {
                Toast.makeText(Login.this, file_url, Toast.LENGTH_SHORT).show();
               /* Snackbar.make(cLayout, "Esto es una prueba", Snackbar.LENGTH_SHORT)
                        .show(); */
            }
        }
    }

}
