package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 09/03/2016.
 */
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sooreck.bistrocabanna.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DialogCambiarPassword extends DialogFragment {


    private Cliente cliente;
    private JSONParser jsonParser = new JSONParser();

    /*Url para actualizar contraseña de la base de datos*/
    private final String UPDATE_PASSWORD_URL = "http://www.mariocastro.96.lt/basedatos/update_password.php";

    // La respuesta del JSON del vector contiene si se completo el proceso de actualizacion y un mensaje;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private static String nuevaPass;

    private static final String TAG = DialogCambiarPassword.class.getSimpleName();

    private static FragmentActivity fa;

    public DialogCambiarPassword() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return crearDialogo();
    }

    public AlertDialog crearDialogo() {


        /* Se crea una alerta de dialogo*/
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        /*Se crea un objeto que nos permitira inflar un layout al Dialog*/

        LayoutInflater inflater = getActivity().getLayoutInflater();

        /*Se infla la vista con el layout coorespondiente para este dialog*/
        View v = inflater.inflate(R.layout.dialog_cambiar_password, null);

        /*Se le asigna la vista*/
        builder.setView(v);

        /*Referencia a los Widget del xml del layout inflado  dialog_cambiar_password */
        Button confirm = (Button) v.findViewById(R.id.confirm_button);
        final EditText antigua = (EditText) v.findViewById(R.id.antigua_password);
        final EditText nueva = (EditText) v.findViewById(R.id.nueva_password);
        final EditText nuevaConfirm = (EditText) v.findViewById(R.id.nueva_password_confirm);

        /*Se obtiene el cliente con el que se inicio sesion, por lo tanto sus datos se encuentran dentro del obj cliente*/
        try {
            Intent i = getActivity().getIntent();
            cliente = (Cliente)i.getSerializableExtra("cliente");
        } catch (Exception ee){

            Log.i("!!!!! ERROR !!!!",".:::::::No obtuvo el cliente::::::::.");
        }


        confirm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /*Se comprueba si hay campos vacios*/
                        if(antigua.getText().toString().equals("") || nueva.getText().toString().equals("") || nuevaConfirm.getText().toString().equals("")){
                            Toast.makeText(getActivity(), "Revisa Campos Vacíos", Toast.LENGTH_SHORT).show();

                        } else /*S no hay vacios se verifica que la contraseña actual es igual a la ingresada*/
                        if (!antigua.getText().toString().equals(cliente.getPassword())){

                            Toast.makeText(getActivity(), "Esta no es tu Password actual", Toast.LENGTH_SHORT).show();

                        } else /*Si era igual se verifica que la nueva password tenga por lo menos 8 digitos*/
                        if (nueva.getText().length() >=8 && nuevaConfirm.getText().length() >= 8) {

                            /*si tiene digitos aceptables se verifica que sean igual la nueva password y la confirmacion*/
                            if (nueva.getText().toString().equals(nuevaConfirm.getText().toString())){

                                nuevaPass = nueva.getText().toString();
                                fa = getActivity();
                                /*Si son iguales se actualiza y manda a llamar la clase que actualiza en la base de datos*/
                                new UpdatePassword().execute(cliente.getUser(), nueva.getText().toString());
                                // Se Actualizan en las preferencias la password para futuros inicios de sesion
                                try {
                                    SharedPreferences sp = getActivity().getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edit = sp.edit();
                                    edit.putString("password",nuevaPass);
                                    edit.putBoolean("sesionGuardada",false);
                                    edit.apply();

                                } catch (NullPointerException esss){
                                    Toast.makeText(getActivity(), "No se obtuvieron Prefs", Toast.LENGTH_SHORT).show();

                                }

                                try {
                                    Intent intent = new Intent(getActivity(), Login.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                                catch (NullPointerException npe){
                                    Log.e("!!! ERROR !!!", "No se lanzo actividad");
                                }
                                dismiss();


                            } else Toast.makeText(getActivity(), "No coinciden las password", Toast.LENGTH_SHORT).show();


                        } /*Si no tiene mas de 8 digitos manda la sugerencia*/
                        else {
                            Toast.makeText(getActivity(), "Tu nueva Password debe tener por lo menos 8 digitos", Toast.LENGTH_LONG).show();

                        }
                    }
                }
        );


        return builder.create();
    }


    class UpdatePassword extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {

            int success;
            String username = args[0];
            String password = args[1];

            try {
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user", username));
                params.add(new BasicNameValuePair("password", password));

                Log.d("request!", "starting");

                JSONObject json = jsonParser.makeHttpRequest(UPDATE_PASSWORD_URL, "POST",
                        params);

                // json success tag
                success = 99;
                try {
                    success = json.getInt(TAG_SUCCESS);
                }
                catch(Exception e){
                    Log.i("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }
                if (success == 1) {

                   /* cliente = new Cliente(cliente.getUser(),nuevaPass,cliente.getName(),
                            cliente.getAddresss(),cliente.getDate(),cliente.getPhone());

                    Intent i = new Intent(getActivity(), ActividadUsuarioPrincipal.class);
                    i.putExtra("cliente", cliente);

                    startActivity(i); */
                    return json.getString(TAG_MESSAGE);
                } else {
                    Log.e("Verifica tus datos", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            if (file_url != null) {
                try {
                    Toast.makeText(fa, file_url, Toast.LENGTH_SHORT).show();
                } catch (Exception es){
                    Log.e("!!ERROR!!", "No se creo el Toast");
                }

            }
        }
    }

}/*Termina la clase DialogCambiarPassword*/
