package com.sooreck.bistrocabanna;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.sooreck.bistrocabanna.Login.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SoOreck on 12/03/2016.
 */
public class SplashScreen extends Activity {

    /*Objeto por el cual recibiremos informacion de la base de datos remota*/
    JSONParser jsonParser = new JSONParser();

    /* URL donde se encuentra el php qeu accede a la base de datos como intermediario*/
    private static final String LOGIN_URL = "http://www.mariocastro.96.lt/basedatos/login.php";

    // La respuesta del JSON del vector contiene si se completo, un mensaje y los datos del cliente
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_DATOS = "datos";

    // TAGS para recuperar informacion del cliente;
    private static final String TAG_USER = "user";
    private static final String TAG_PASSWORD = "password";
    private static final String TAG_NAME = "name";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_DATE = "date";
    private static final String TAG_PHONE = "phone";

    /* Objeto donde se guardara informacion recuperada de bd remota*/
    public Cliente cliente;

    /*Recuperar el JSONArray del JSONObjet*/
    private JSONArray datosCliente = null;

    private boolean sesionGuardada;
    private Thread timerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        SharedPreferences prefs =  getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
        sesionGuardada = prefs.getBoolean("sesionGuardada",false);

        if (sesionGuardada){
            if (tieneConexion(SplashScreen.this)) {
                try {
                    new AttemptLogin().execute(prefs.getString("username", ""), prefs.getString("password",""));
                } catch(Exception e){
                   e.printStackTrace();
                    Toast.makeText(SplashScreen.this, "No existe conexion", Toast.LENGTH_SHORT).show();
                }

            } else {
               /*
                Para pruebas
                Intent intent = new Intent(SplashScreen.this, Exp.class);
                startActivity(intent); */

                Intent intent = new Intent(SplashScreen.this, Login.class);
                intent.putExtra("tieneDatos",true);
                intent.putExtra("tieneConexion",tieneConexion(SplashScreen.this));
                startActivity(intent);
            }


        } else {
             timerThread = new Thread(){
                public void run(){
                    try{
                        sleep(3000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        Intent intent = new Intent(SplashScreen.this, Login.class);
                        intent.putExtra("tieneDatos",false);
                        startActivity(intent);
                    }
                }
            };
            timerThread.start();
        }

        startService(new Intent(this, RegistrationIntentService.class));

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void iniciaActividadInicio(){

        /*Se inicia la actividad y se envia el objeto cliente*/
        Intent i = new Intent(SplashScreen.this, ActividadUsuarioPrincipal.class);
        i.putExtra("cliente", cliente);
        finish();
        startActivity(i);
    }

    public static boolean tieneConexion(Context context) {

        boolean connected = false;

        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Recupera todas las redes (tanto móviles como wifi)
        NetworkInfo[] redes = connec.getAllNetworkInfo();

        for (int i = 0; i < redes.length; i++) {
            // Si alguna red tiene conexión, se devuelve true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                connected = true;
            }
        }
        return connected;
    }// Fin del metodo tiene conexion


    /* Clase para tarea asincrona
   *  En esta clase se inicia sesion y se recuperan datos del JSON*/
    public class AttemptLogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            /*Antes de ejecutarse se mostrara un dialogo*/

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            /*Se realiza en segundo plano mientras se muestra el dialog de iniciando sesion pDialog */

            int success; /*Nos permitira saber si el login fue correcto*/

            /*Se obtienen los argumentos enviados al ejecutar la tarea asincrona
            * args[0] para el usuario
            * args[1] para la password */
            String username = args[0];
            String password = args[1];

            try {

                /* Se crean los parametros que se enviaran por POST */
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user", username));
                params.add(new BasicNameValuePair("password", password));

                Log.d("request!", "starting");

                // Obtiene el objeto JSON del HTTP request  *** Revisar Clase JSONParser ***
                /* Se envian por POST */
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",
                        params);

                /*Se obitene del objeto JSON el TAG_SUCCESS para saber si se puede iniciar sesion o no*/
                success = 99;
                try {
                    success = json.getInt(TAG_SUCCESS);
                }

                catch(Exception e){

                    Log.d("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
                }

                /*Si la base de datos dio acceso de login*/
                if (success == 1) {
                    Log.d("Login Correcto", json.toString());

                    // Se guardan en las preferencias el usuario y password para futuros inicios de sesion
                    SharedPreferences sp = getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);

                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString("username", username);
                    edit.putString("password",password);
                    edit.putBoolean("sesionGuardada",true);
                    edit.apply();

                    /*Se almacenaran los datos recibidos del jsonarray*/
                    ArrayList<String> list = new ArrayList<>();

                    /*del JSON array se obtiene el vector de datos para crear un nuevo Objeto tipo Usuario*/
                    datosCliente = json.getJSONArray(TAG_DATOS);

                    String userExt = "";
                    String passwordExt = "";
                    String nameExt = "";
                    String addressExt = "";
                    String dateExt = "";
                    String phoneExt = "";

                    if (datosCliente != null) {

                        for (int i = 0; i < datosCliente.length(); i++) {
                        /*Se obtiene cada dato del json para extrar los datos del usuario
                        * Se extraen
                        * Se añaden a una lista
                        *
                        * */

                            JSONObject c = datosCliente.getJSONObject(i);

                            userExt = c.getString(TAG_USER);
                            list.add(userExt);
                            Log.e("!!!!!DATOS!!!!!", userExt);

                            passwordExt = c.getString(TAG_PASSWORD);
                            list.add(passwordExt);
                            Log.e("!!!!!DATOS!!!!!", passwordExt);

                            nameExt = c.getString(TAG_NAME);
                            list.add(nameExt);
                            Log.e("!!!!!DATOS!!!!!", nameExt);

                            addressExt = c.getString(TAG_ADDRESS);
                            list.add(addressExt);
                            Log.e("!!!!!DATOS!!!!!", addressExt);

                            dateExt = c.getString(TAG_DATE);
                            list.add(dateExt);
                            Log.e("!!!!!DATOS!!!!!", dateExt);

                            phoneExt = c.getString(TAG_PHONE);
                            list.add(phoneExt);
                            Log.e("!!!!!DATOS!!!!!", phoneExt);

                        }

                    }

                    /* Se crea el objeto tipo cliente con los datos con el que se inicio sesion*/
                    cliente = new Cliente(userExt,passwordExt,nameExt,addressExt,dateExt,phoneExt);


                    return json.getString(TAG_MESSAGE);
                }

                /*Si la base de datos no dio acceso de login*/
                else {
                    Log.d("Verifica tus datos", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            /* Despues de terminar el backgroud se cierra el pDialog y se muestra un Toast si el login fue correcto o no*/

            if (file_url != null) {
                Toast.makeText(SplashScreen.this, file_url, Toast.LENGTH_SHORT).show();
               /* Snackbar.make(cLayout, "Esto es una prueba", Snackbar.LENGTH_SHORT)
                        .show(); */
                iniciaActividadInicio();
            }
        }
    }


}