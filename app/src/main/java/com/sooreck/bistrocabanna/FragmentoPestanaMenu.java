package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 06/03/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragmento que representara el contenido de cada una de las pestaña dentro de la sección "Menu"
 */
public class FragmentoPestanaMenu extends Fragment {

    private static final String INDICE_SECCION = "com.sooreck.bistrocabanna.FragmentoPestanaMenu.extra.INDICE_SECCION";

    private RecyclerView reciclador;
    private GridLayoutManager layoutManager;
    private AdaptadorMenu adaptador;

    private Cliente cliente;

    public static FragmentoPestanaMenu nuevaInstancia(int indiceSeccion) {
        FragmentoPestanaMenu fragment = new FragmentoPestanaMenu();
        Bundle args = new Bundle();
        args.putInt(INDICE_SECCION, indiceSeccion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_pestana_menu, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        reciclador.setLayoutManager(layoutManager);

        int indiceSeccion = getArguments().getInt(INDICE_SECCION);

        switch (indiceSeccion) {
            case 0:
                adaptador = new AdaptadorMenu(Comida.PLATILLOS);

                adaptador.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent idatos = getActivity().getIntent();
                        cliente = (Cliente)idatos.getSerializableExtra("cliente");

                        int posicion = reciclador.getChildAdapterPosition(v);

                        if (posicion == 0){

                            Intent i = new Intent(getActivity(), Hamburguesa.class);
                            i.putExtra("infoCliente", cliente);
                            i.putExtra("platillo",Comida.PLATILLOS.get(posicion).getNombre());
                            i.putExtra("ingredientes", "Especialidad de la casa");
                            i.putExtra("precio", Comida.PLATILLOS.get(posicion).getPrecio());
                            i.putExtra("imagen", Comida.PLATILLOS.get(posicion).getIdDrawable());
                            startActivity(i);
                        }
                        if (posicion == 1) {

                            Intent i = new Intent(getActivity(), salad.class);
                            i.putExtra("infoCliente", cliente);
                            i.putExtra("platillo",Comida.PLATILLOS.get(posicion).getNombre());
                            i.putExtra("ingredientes", "Especialidad de la casa");
                            i.putExtra("precio", Comida.PLATILLOS.get(posicion).getPrecio());
                            i.putExtra("imagen", Comida.PLATILLOS.get(posicion).getIdDrawable());
                            startActivity(i);

                        }
                        if (posicion != 0 && posicion !=1) {
                            Intent i = new Intent(getActivity(), ActividadAgregarCarrito.class);
                            i.putExtra("infoCliente", cliente);
                            i.putExtra("platillo",Comida.PLATILLOS.get(posicion).getNombre());
                            i.putExtra("ingredientes", "Especialidad de la casa");
                            i.putExtra("precio", Comida.PLATILLOS.get(posicion).getPrecio());
                            i.putExtra("imagen", Comida.PLATILLOS.get(posicion).getIdDrawable());
                            startActivity(i);

                        }
                    }
                });

                break;
            case 1:
                adaptador = new AdaptadorMenu(Comida.BEBIDAS);

                adaptador.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent idatos = getActivity().getIntent();
                        cliente = (Cliente)idatos.getSerializableExtra("cliente");

                        int posicion = reciclador.getChildAdapterPosition(v);

                        Intent i = new Intent(getActivity(), ActividadAgregarCarrito.class);
                        i.putExtra("infoCliente", cliente);
                        i.putExtra("platillo",Comida.BEBIDAS.get(posicion).getNombre());
                        i.putExtra("ingredientes","Especialidad de la casa");
                        i.putExtra("precio",Comida.BEBIDAS.get(posicion).getPrecio());
                        i.putExtra("imagen",Comida.BEBIDAS.get(posicion).getIdDrawable());
                        startActivity(i);
                    }
                });
                break;
            case 2:
                adaptador = new AdaptadorMenu(Comida.POSTRES);

                adaptador.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent idatos = getActivity().getIntent();
                        cliente = (Cliente)idatos.getSerializableExtra("cliente");

                        int posicion = reciclador.getChildAdapterPosition(v);

                        Intent i = new Intent(getActivity(), ActividadAgregarCarrito.class);
                        i.putExtra("infoCliente", cliente);
                        i.putExtra("platillo",Comida.POSTRES.get(posicion).getNombre());
                        i.putExtra("ingredientes","Especialidad de la casa");
                        i.putExtra("precio",Comida.POSTRES.get(posicion).getPrecio());
                        i.putExtra("imagen",Comida.POSTRES.get(posicion).getIdDrawable());
                        startActivity(i);
                    }
                });
                break;
        }

        reciclador.setAdapter(adaptador);

        return view;
    }


}