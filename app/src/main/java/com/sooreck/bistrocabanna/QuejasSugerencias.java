package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 22/03/2016.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Clase de  para poblar el fragmento donde el cliente podra dar y ver quejas y sugerencias de otros usuarios.
 * Contiene el usuario que publica
 * Contiene el comentario que quiere dejar
 * Contiene fecha de cuando se publico
 * Contiene el rating que el usuario dejo varia entre 1 y 5
 */

public class QuejasSugerencias {

    private String user;
    private String comentario;
    private String fecha;
    private int rating;


    /*Constructor para inicializar objetos con algunos valroes por default usado para crear lo obtenidos de la BD*/
    public QuejasSugerencias(String user, String comentario, String fecha, int rating){

        this.user = user;
        this.comentario = comentario;
        this.fecha = fecha;
        this.rating = rating;
    }

    /*Lista para poblar el adaptadro Por default*/
    public static final List<QuejasSugerencias> COMENTARIOS = new ArrayList<>();

    static {
        COMENTARIOS.add(new QuejasSugerencias("Mario Castro","Bistro Cabanna el mejor lugar para disfrutar de una comida esquisita",
                "22/03/2016",5));
        COMENTARIOS.add(new QuejasSugerencias("Yair Herrera","Excelente servicio al cliente",
                "21/03/2016",4));
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
