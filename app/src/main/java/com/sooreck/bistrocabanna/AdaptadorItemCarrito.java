package com.sooreck.bistrocabanna;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SoOreck on 30/03/2016.
 */
public class AdaptadorItemCarrito extends RecyclerView.Adapter<AdaptadorItemCarrito.ViewHolder> {


    private List<ItemCarrito> items;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView platillo;
        public TextView direccion;
        public TextView precio;
        public TextView ingredientes;



        public ViewHolder(View v) {
            super(v);
            platillo = (TextView) v.findViewById(R.id.texto_platillo_carrito);
            direccion = (TextView) v.findViewById(R.id.texto_dir_carrito);
            precio = (TextView) v.findViewById(R.id.texto_precio_carrito);
            ingredientes = (TextView) v.findViewById(R.id.texto_ingredientes_carrito);
        }
    }

    public AdaptadorItemCarrito(List<ItemCarrito> items) {
        this.items = items;
    }

    @Override
    public AdaptadorItemCarrito.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_carrito, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorItemCarrito.ViewHolder holder, int position) {
        holder.platillo.setText("Platillo: "+items.get(position).getPlatillo());
        holder.direccion.setText("Direccion: "+items.get(position).getDireccion());
        holder.precio.setText("Precio: "+items.get(position).getPreciototal());
        holder.ingredientes.setText("Ingredientes: "+items.get(position).getIngredientes());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
