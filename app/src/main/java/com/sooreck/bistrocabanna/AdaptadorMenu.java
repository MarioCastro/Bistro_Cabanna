package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 06/03/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sooreck.bistrocabanna.R;


import java.util.List;

/**
 * Adaptador para comidas usadas en la sección "Categorías"
 */
public class AdaptadorMenu extends RecyclerView.Adapter<AdaptadorMenu.ViewHolder> implements View.OnClickListener {


    private final List<Comida> items;
    private View.OnClickListener listener;


    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView nombre;
        public TextView precio;
        public ImageView imagen;
        public ImageView editable;
        public View view;


        public ViewHolder(View v) {
            super(v);

            nombre = (TextView) v.findViewById(R.id.nombre_comida);
            precio = (TextView) v.findViewById(R.id.precio_comida);
            imagen = (ImageView) v.findViewById(R.id.miniatura_comida);
            editable = (ImageView) v.findViewById(R.id.img_edit);
            view = v;



        }
    }


    public AdaptadorMenu(List<Comida> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_lista_menu, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Comida item = items.get(i);

        Glide.with(viewHolder.itemView.getContext())
                .load(item.getIdDrawable())
                .centerCrop()
                .into(viewHolder.imagen);

       Glide.with(viewHolder.itemView.getContext())
               .load(item.getEditable())
               .override(54  , 54)
               .into(viewHolder.editable);

        viewHolder.itemView.setOnClickListener(this);



        //viewHolder.editable.setImageDrawable(viewHolder.itemView.getContext().getResources().getDrawable(item.getEditable()));

        viewHolder.nombre.setText(item.getNombre());

        /*todo verificar*/
        if(item.getPrecio().equals("Platillo Preparable")) {
            viewHolder.precio.setText(item.getPrecio());
        } else viewHolder.precio.setText("$"+item.getPrecio());

        /*
        if (i == 0 || i == 1) {
            viewHolder.precio.setText(item.getPrecio());
        } else
        viewHolder.precio.setText("$"+item.getPrecio()); */

    }



}