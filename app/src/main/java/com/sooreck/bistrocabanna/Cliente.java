package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 29/02/2016.
 */

import java.io.Serializable;

@SuppressWarnings("serial")
public class Cliente implements Serializable {


    String user;
    String password;
    String name;
    String addresss;
    String date;
    String phone;


    public Cliente(String userPost, String passwordPost, String namePost, String addresssPost,
                    String datePost, String phonePost)
    {
        this.user = userPost;
        this.password = passwordPost;
        this.name = namePost;
        this.addresss = addresssPost;
        this.date = datePost;
        this.phone = phonePost;


    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddresss() {
        return addresss;
    }

    public void setAddresss(String addresss) {
        this.addresss = addresss;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }





}
