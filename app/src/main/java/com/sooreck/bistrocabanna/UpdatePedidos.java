package com.sooreck.bistrocabanna;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SoOreck on 10/04/2016.
 */
public class UpdatePedidos extends AsyncTask<String, String, String> {

    private Cliente cliente;
    private JSONParser jsonParser = new JSONParser();

    /*Url para actualizar el estado de los pedidos que estan a espera de ser pagados*/
    private final String URL = "http://www.mariocastro.96.lt/basedatos/update_pedidos.php";

    // La respuesta del JSON del vector contiene si se completo el proceso de actualizacion y un mensaje;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... args) {
        int success;
        String username = args[0];
        String status = args[1];

        try {
            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("status", status));

            Log.d("request!", "starting");

            JSONObject json = jsonParser.makeHttpRequest(URL, "POST",
                    params);

            // json success tag
            success = 99;
            try {
                success = json.getInt(TAG_SUCCESS);
            }
            catch(Exception e){
                Log.i("!!!!!!!!!NULL!!!!",".:::::::ERROR::::::::.");
            }
            if (success == 1) {

                   /* cliente = new Cliente(cliente.getUser(),nuevaPass,cliente.getName(),
                            cliente.getAddresss(),cliente.getDate(),cliente.getPhone());

                    Intent i = new Intent(getActivity(), ActividadUsuarioPrincipal.class);
                    i.putExtra("cliente", cliente);

                    startActivity(i); */
                return json.getString(TAG_MESSAGE);
            } else {
                Log.e("ERROR", json.getString(TAG_MESSAGE));
                return json.getString(TAG_MESSAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

    }


}
