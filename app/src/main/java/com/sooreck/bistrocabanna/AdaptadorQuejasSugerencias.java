package com.sooreck.bistrocabanna;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;


/**
 * Created by SoOreck on 22/03/2016.
 */
public class AdaptadorQuejasSugerencias extends RecyclerView.Adapter<AdaptadorQuejasSugerencias.ViewHolder>  {

    private List<QuejasSugerencias> items;

    public AdaptadorQuejasSugerencias(List<QuejasSugerencias> items) {
        this.items = items;
    }

    @Override
    public AdaptadorQuejasSugerencias.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_comentarios, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       holder.usuario.setText(items.get(position).getUser()+" dice:");
       holder.comentario.setText(items.get(position).getComentario());
       holder.fecha.setText(items.get(position).getFecha());
        holder.rating.setRating(items.get(position).getRating());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView usuario;
        public TextView comentario;
        public TextView fecha;
        public RatingBar rating;


        public ViewHolder(View v) {
            super(v);
            usuario = (TextView) v.findViewById(R.id.usuario);
            comentario = (TextView) v.findViewById(R.id.comentario);
            fecha = (TextView) v.findViewById(R.id.fecha);
            rating = (RatingBar) v.findViewById(R.id.rating);
        }
    }



}//Termina la clase AdaptadorQuejasSugerencias
