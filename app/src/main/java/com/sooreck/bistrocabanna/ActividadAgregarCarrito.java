package com.sooreck.bistrocabanna;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ActividadAgregarCarrito extends AppCompatActivity {

    private TextView tvPlatillo;
    private TextView tvIngredientes;
    private TextView tvPrecio;
    private TextView tvDireccion;
    private TextView tvNombre;

    private ImageView imgPlatillo;
    private ImageView imageViewEditDir;

    /*Atributos para la Animacion*/
    private ViewGroup linearLayoutDetails;
    private ImageView imageViewExpand;
    private static final int DURATION = 250;

    private Cliente cliente;

    // JSON  class
    JSONParser jsonParser = new JSONParser();

    private static final String URL = "http://mariocastro.96.lt/basedatos/insert_pedido.php";

    //ids
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_agregar_carrito);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvPlatillo = (TextView) findViewById(R.id.texto_platillo);
        tvIngredientes = (TextView) findViewById(R.id.textViewIngredientes);
        tvPrecio = (TextView) findViewById(R.id.texto_precio);
        tvDireccion = (TextView) findViewById(R.id.texto_direccion);
        tvNombre = (TextView) findViewById(R.id.texto_nombre);


        imgPlatillo = (ImageView) findViewById(R.id.img_platillo);
        imageViewEditDir = (ImageView) findViewById(R.id.imageViewEditDir);

        linearLayoutDetails = (ViewGroup) findViewById(R.id.linearLayoutIngredientes);
        imageViewExpand = (ImageView) findViewById(R.id.imageViewExpandIngredientes);

        llenarInformacion();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent idatos = getIntent();
                cliente = (Cliente)idatos.getSerializableExtra("infoCliente");
                new InsertPedido().execute(cliente.getUser(), tvDireccion.getText().toString(), idatos.getStringExtra("platillo"),
                        idatos.getStringExtra("ingredientes"), idatos.getStringExtra("precio"), "Pendiente");
                setResult(Activity.RESULT_OK);
                finish();



            }
        });

        imageViewEditDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Edita tu direccion", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(ActividadAgregarCarrito.this,MapsActivity.class);
                startActivityForResult(intent, 1);
                Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vib.vibrate(50);
            }
        });

    }

    private void llenarInformacion(){
        Intent idatos = getIntent();
        cliente = (Cliente)idatos.getSerializableExtra("infoCliente");
        Log.e("ATENCION",cliente.getAddresss() + " " + cliente.getName());
        tvDireccion.setText(cliente.getAddresss());
        tvNombre.setText("Para: " + cliente.getName());

        tvPlatillo.setText(idatos.getStringExtra("platillo"));
        tvIngredientes.setText(idatos.getStringExtra("ingredientes"));
        tvPrecio.setText("Precio: " + idatos.getStringExtra("precio"));

        Glide.with(ActividadAgregarCarrito.this)
        .load(idatos.getIntExtra("imagen",0))
        .into(imgPlatillo);

    }

    /*Metodo para la animacion y para mostrar detalles del cardiew*/
    public void toggleDetails(View view) {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.indicador_abajo);
            rotate(-180.0f);

        } else {
            ExpandAndCollapseViewUtil.collapse(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.indicador_arriba);
            rotate(90.0f);

        }
    }
    /* Metodo para rotar la animcaion de una imagen */
    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        imageViewExpand.startAnimation(animation);
    }


    /***********************************************************************************************
     *************************** RECIBE NUEVA DIRECCION DE OTRA ACTIVIDAD **************************
     * ********************************************************************************************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Request code para identificar cual actividad regresa un valor
        //Result code para saber si fue resultado ok o resultado cancelado
        if (requestCode == 1){

            if (resultCode == Activity.RESULT_OK){
                String resultado = data.getStringExtra("newaddress");
                tvDireccion.setText(resultado);
                Toast.makeText(ActividadAgregarCarrito.this, "Dirección cambiada", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(ActividadAgregarCarrito.this, "Dirección no cambiada", Toast.LENGTH_SHORT).show();
            }

        }

    }//Termina metodo Override onActivityResult

    /***********************************************************************************************
     ************************* CLASE ASINCRONA PARA INSERTAR PEDIDO EN BD **************************
     * ********************************************************************************************/

    class InsertPedido extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {

            // Check for success tag
            int success;

            String username = args[0];
            String address =args[1];
            String platillo = args[2];
            String ingredientes = args[3];
            String preciototal = args[4];
            String status = args[5];

            try {
                // Building Parameters
                List params = new ArrayList();
                params.add(new BasicNameValuePair("username", username));
                params.add(new BasicNameValuePair("address", address));
                params.add(new BasicNameValuePair("platillo", platillo));
                params.add(new BasicNameValuePair("ingredientes", ingredientes));
                params.add(new BasicNameValuePair("preciototal", preciototal));
                params.add(new BasicNameValuePair("status", status));



                Log.d("request!", "starting");

                //Posting user data to script
                JSONObject json = jsonParser.makeHttpRequest(
                        URL, "POST", params);

                // full json response
                Log.d("Registrando intento", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Log.d("Pedido enviado", json.toString());
                    insertarEnCarrito();
                    return json.getString(TAG_MESSAGE);
                }else{
                    Log.d("FalloInsertarPedido", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            if (file_url != null){
                Toast.makeText(ActividadAgregarCarrito.this, file_url, Toast.LENGTH_LONG).show();
            }
        }
    }// Termina la clase para agregar pedido a la base de datos

    private void insertarEnCarrito(){
        SharedPreferences preferences = getSharedPreferences("CarritoUser", Context.MODE_PRIVATE);
        Gson gson = new Gson();

                /*Se obtiene el carrito del usuario para agregar nuevo item*/
        String saveList = preferences.getString("carrito", "");
        List<ItemCarrito> carrito;
        if (saveList.equals("")){
            carrito = new ArrayList<>();
        } else {
                   /*Conseguir el tipo de dato para que gson sepa a que tipo de dato convetir*/
            Type type = new TypeToken<List<ItemCarrito>>(){}.getType();
                /* Convertimos el string en el list*/
            carrito = gson.fromJson(saveList,type);
        }

        Intent idatos = getIntent();
        cliente = (Cliente)idatos.getSerializableExtra("infoCliente");
        ItemCarrito pedidoAgregarCarrito = new ItemCarrito(idatos.getStringExtra("platillo"),tvDireccion.getText().toString(),
                "now()","Pendiente", idatos.getStringExtra("precio"),
                cliente.getUser(), idatos.getStringExtra("ingredientes"));
        carrito.add(pedidoAgregarCarrito);
        String jsonList = gson.toJson(carrito);

        // Se guardan en las preferencias un item de carrito para despues guardar mas o eliminarlo
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("carrito", jsonList);

        float total = preferences.getFloat("total", 0f);
        total += Float.parseFloat(idatos.getStringExtra("precio"));

        editor.putFloat("total",total);

        editor.apply();

    }


}// Termina Clase ActividadAgregarCarrito
