package com.sooreck.bistrocabanna;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.util.Vector;

public class salad extends AppCompatActivity {





    public int SelectedImage, posicionPrecio = 0;
    public boolean focus = true;
    Animation inSuma, outSuma, inResta, outResta;
    TextSwitcher textSwitcher;
    private ViewGroup marco;
    public TextSwitcher price;
    double precioTotal = 0.0;
    ImageView DrawImage;
    Rect rect;
    Vector<String> V1 = new Vector <String> ();
    Vector <Integer> V2 = new Vector <Integer> ();
    Vector <Double> vectorPrecios = new Vector <Double> ();

    /**********************************************************************************************
     ******************************************  MÉTODOS ******************************************
     **********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salad);

        /****************************** IMAGENES CON LONGCLICKLISTENER *****************************/
        LonkClickListeners();

        /************************************* BOTON TERMINAR **************************************/
        BotonTerminar();

        /********************************* AGREGAR PRECIO INICIAL **********************************/
        vectorPrecios.add(precioTotal);
        init();
        setFactory();

        /****************************************** LAYOUT *****************************************/
        //Identificar el Drop Area
        findViewById(R.id.dropArea).setOnDragListener(DropListener);
        marco = (ViewGroup)findViewById(R.id.dropArea);

    }





    /**********************************************************************************************
     ************************************  Dibujar Hamburguesa  ***********************************
     **********************************************************************************************/

    public void Dibujar_y_Almacenar_Ingrediente(final float x, float y)
    {

        //Se genera una ID aleatoria de 8 digitos para el View seleccinado
        int value; String key = "";
        for (int i = 0; i < 8; i++)
        {
            value = (int)(Math.random() * 9 + 1);
            key += value;
        }
        //Se convierte el String a valor numérico
        int ID = Integer.parseInt(key);

        //Tamaño Imagen
        ImageView dimensiones = (ImageView)findViewById(SelectedImage); //Sacar Imagen Seleccionada
        int width = dimensiones.getWidth();   //Ancho Imagen Seleccionada
        int height = dimensiones.getHeight();  //Ancho Imagen Seleccionada

        //Layout
        AbsoluteLayout absoluteLayout = (AbsoluteLayout)findViewById(R.id.dropArea);
        DrawImage = new ImageView(this);  //Imagen que se va a dibujar

        if (R.id.lechuga == SelectedImage) {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.lechuga2));
            V1.add("Lechuga");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);
            width = dimensiones.getWidth()*2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()*2; //Alto Imagen Seleccionada
        }
        else if (R.id.pepino == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.pepino2));
            V1.add("Pepino");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(3.0);
        }
        else if (R.id.aceituna == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.aceitunanegra));
            V1.add("Aceituna");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/3;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/3; //Alto Imagen Seleccionada
        }
        else if (R.id.kiwi == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.kiwi1));
            V1.add("Kiwi");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);
            width = dimensiones.getWidth()/3;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/3; //Alto Imagen Seleccionada
        }
        else if (R.id.cebolla == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.cebolla2));
            V1.add("Cebolla");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.tomate == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.stomate1));
            V1.add("Tomate");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.zanahoria == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.zanahoria1));
            V1.add("Zanahoria");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(3.0);
        }
        else if (R.id.huevo == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.huevo2));
            V1.add("Huevo");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(3.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.berenjena == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.berenjena1));
            V1.add("Berenjena");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }
        else if (R.id.queso == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.squeso1));
            V1.add("Queso");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.champ == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.mushrom));
            V1.add("Champiñon");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.tocino == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.tocino1));
            V1.add("Tocino");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);
        }
        else if (R.id.naranja == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.naranja1));
            V1.add("Naranja");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(3.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.mora == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.mora1));
            V1.add("Mora");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.platano == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.platano1));
            V1.add("Platano");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.frambuesa == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.frambuesa));
            V1.add("Frambuesa");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.fresa == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.fresa1));
            V1.add("Fresa");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(3.0);
            width = dimensiones.getWidth()/2;   //Ancho Imagen Seleccionada
            height = dimensiones.getHeight()/2; //Alto Imagen Seleccionada
        }
        else if (R.id.chileamarillo == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.chileamraillo1));
            V1.add("Chile Amarillo");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.50);
        }
        else if (R.id.chilerojo == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.chilerojo1));
            V1.add("Chile Rojo");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.50);
        }
        else if (R.id.chipotle == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.chipotle2));
            V1.add("Chipotle");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }
        else if (R.id.cesar == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.caesar2));
            V1.add("Caesar");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }
        else if (R.id.ranch == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.ranch2));
            V1.add("Ranch");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }
        else if (R.id.berry == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.berry2));
            V1.add("Berry");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }
        else if (R.id.honey == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.honey2));
            V1.add("Honey");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(5.0);
        }


        final int Posx = (int) (x-width);  //Pos X  donde se va a dibujar la imagen
        final int Posy = (int) (y-height); //Pos Y  donde se va a dibujar la imagen

        //Parametros para dibujar la imagen
        final AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams(width+20,height+20,Posx,Posy); //(Ancho , Altura , x , y)

        DrawImage.setLayoutParams(layoutParams);//Pasar los parametros a la imagen Seleccionada
        absoluteLayout.addView(DrawImage);     //Añadir un View al Layout con la imagen Seleccionada


        //Llamar método de Arrastrar ingredientes ya agregados
        ArrastrarIngrediente(absoluteLayout, DrawImage);

    }



    public void ArrastrarIngrediente(final AbsoluteLayout absoluteLayout, final ImageView ingrediente)
    {

        final ImageView trashcan = (ImageView)findViewById(R.id.trashcan);

        // /Arrstrar Ingrediente
        ingrediente.setOnTouchListener(new View.OnTouchListener() {

                                           float lastTouchX, lastTouchY; //ultima posicion  de X y Y
                                           float posX, posY; //Posicion de X y Y general
                                           float posAnteriorX, posAnteriorY; //Posicion anterior de X y Y
                                           float posActualX, posActualY;  //Posicion Actual de X y Y

                                           @Override
                                           public boolean onTouch(View v, MotionEvent event) {

                                               switch (event.getAction()) {
                                                   case MotionEvent.ACTION_DOWN: {
                                                       //Al presionar se guarda la ultima posicion
                                                       lastTouchX = event.getX();
                                                       lastTouchY = event.getY();
                                                       break;
                                                   }
                                                   case MotionEvent.ACTION_UP: {

                                                       int RawX = (int)event.getRawX();
                                                       int RawY = (int)event.getRawY();

                                                       if(Elimino(trashcan,RawX,RawY))
                                                       {
                                                           EliminarIngrediente(ingrediente);
                                                       }

                                                       break;
                                                   }

                                                   case MotionEvent.ACTION_MOVE: {
                                                       posActualX = event.getX() - lastTouchX;
                                                       posActualY = event.getY() - lastTouchY;

                                                       posX = posAnteriorX + posActualX;
                                                       posY = posAnteriorY + posActualY;

                                                       if (posX > 0 && posY > 0 && (posX + v.getWidth()) < absoluteLayout.getWidth() && (posY + v.getHeight()) < absoluteLayout.getHeight()) {
                                                           v.setLayoutParams(new AbsoluteLayout.LayoutParams(v.getMeasuredWidth(), v.getMeasuredHeight(), (int) posX, (int) posY));

                                                           posAnteriorX = posX;
                                                           posAnteriorY = posY;

                                                       }

                                                       break;
                                                   }
                                               }
                                               return true;
                                           }
                                       }

        );

    }



    public boolean Elimino(View view, int x, int y){

        rect = new Rect();
        int [] location = new int[2];

        view.getDrawingRect(rect);
        view.getLocationOnScreen(location);
        rect.offset(location[0], location[1]);
        boolean Flag = rect.contains(x, y);

        return Flag;
    }






    /*********************************************************************************************
     ******************************************           *****************************************
     ******************************************  EVENTOS  *****************************************
     ******************************************           *****************************************
     **********************************************************************************************/

    /**********************************************************************************************
     ***********************************  Long Click Listeners  ***********************************
     **********************************************************************************************/
    public void LonkClickListeners()
    {
        findViewById(R.id.lechuga).setOnLongClickListener(longClickListener);
        findViewById(R.id.pepino).setOnLongClickListener(longClickListener);
        findViewById(R.id.aceituna).setOnLongClickListener(longClickListener);
        findViewById(R.id.kiwi).setOnLongClickListener(longClickListener);
        findViewById(R.id.cebolla).setOnLongClickListener(longClickListener);
        findViewById(R.id.tomate).setOnLongClickListener(longClickListener);
        findViewById(R.id.zanahoria).setOnLongClickListener(longClickListener);
        findViewById(R.id.huevo).setOnLongClickListener(longClickListener);
        findViewById(R.id.berenjena).setOnLongClickListener(longClickListener);
        findViewById(R.id.queso).setOnLongClickListener(longClickListener);
        findViewById(R.id.champ).setOnLongClickListener(longClickListener);
        findViewById(R.id.tocino).setOnLongClickListener(longClickListener);
        findViewById(R.id.naranja).setOnLongClickListener(longClickListener);
        findViewById(R.id.mora).setOnLongClickListener(longClickListener);
        findViewById(R.id.platano).setOnLongClickListener(longClickListener);
        findViewById(R.id.frambuesa).setOnLongClickListener(longClickListener);
        findViewById(R.id.fresa).setOnLongClickListener(longClickListener);
        findViewById(R.id.chileamarillo).setOnLongClickListener(longClickListener);
        findViewById(R.id.chilerojo).setOnLongClickListener(longClickListener);
        findViewById(R.id.chipotle).setOnLongClickListener(longClickListener);
        findViewById(R.id.cesar).setOnLongClickListener(longClickListener);
        findViewById(R.id.ranch).setOnLongClickListener(longClickListener);
        findViewById(R.id.berry).setOnLongClickListener(longClickListener);
        findViewById(R.id.honey).setOnLongClickListener(longClickListener);
    }



    /**********************************************************************************************
     ********************************  Eliminar Ingrediente Vector  *******************************
     **********************************************************************************************/
    public void EliminarIngrediente(View v)
    {
        int tam = V2.size();
        int ide = v.getId();

        for (int i = 0; i < tam; i++)
        {
            if (V2.elementAt(i)==(ide))
            {
                //String msg = "Ingrediente Eliminado: "+String.valueOf(ide)+" - "+String.valueOf(V1.elementAt(i));
                restaIngrediente(V1.elementAt(i));
                V1.remove(i);
                V2.remove(i);
                v.setVisibility(View.INVISIBLE);
                //Toast.makeText(getApplicationContext(),msg ,Toast.LENGTH_SHORT).show();
                i = tam + 1;
            }
        }

    }


    /**********************************************************************************************
     *********************************  Mostrar Ingrediente Vector  *******************************
     **********************************************************************************************/
    public String ImprimirIngredientes()
    {
        int tam = V1.size();
        String nuevo;
        String ingredientes = "";

        for (int i = 0; i < tam; i++)
        {
            nuevo = V1.elementAt(i) + ", ";
            ingredientes += nuevo;
        }

        return ingredientes;
    }


    /**********************************************************************************************
     ***************************************  Boton Terminar  *************************************
     **********************************************************************************************/
    public void BotonTerminar()
    {
        final Button finish = (Button)findViewById(R.id.terminar);

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (V1.size() == 0){
                    Toast.makeText(getApplicationContext(),"No has colocado ningun ingrediente",Toast.LENGTH_SHORT).show();
                } else
                if (V1.size() > 0 && V1.size() < 4){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(salad.this);
                    alertDialogBuilder.setTitle("Bistro Cabanna");
                    alertDialogBuilder.setIcon(R.drawable.ic_doulbecheck);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Muy Pocos Ingredientes, ¿Desea Continuar?")
                            .setCancelable(false)
                            .setPositiveButton("SI",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {

                                    String ingredients = ImprimirIngredientes();
                                    Toast.makeText(getApplicationContext(),ingredients,Toast.LENGTH_SHORT).show();
                                    Intent idatos = getIntent();
                                    Cliente cliente = (Cliente)idatos.getSerializableExtra("infoCliente");

                                    Intent i = new Intent(salad.this, ActividadAgregarCarrito.class);
                                    i.putExtra("infoCliente", cliente);
                                    i.putExtra("platillo",idatos.getStringExtra("platillo"));
                                    i.putExtra("ingredientes", ingredients);
                                    i.putExtra("precio", String.valueOf(precioTotal));
                                    i.putExtra("imagen", idatos.getIntExtra("imagen",0));
                                    startActivityForResult(i, 100);

                                }
                            })
                            .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {
                    String ingredients = ImprimirIngredientes();

                    Intent idatos = getIntent();
                    Cliente cliente = (Cliente)idatos.getSerializableExtra("infoCliente");

                    Intent i = new Intent(salad.this, ActividadAgregarCarrito.class);
                    i.putExtra("infoCliente", cliente);
                    i.putExtra("platillo",idatos.getStringExtra("platillo"));
                    i.putExtra("ingredientes", ingredients);
                    i.putExtra("precio", String.valueOf(precioTotal));
                    i.putExtra("imagen", idatos.getIntExtra("imagen",0));
                    startActivityForResult(i, 100);
                }
            }
        });
    }



    /**********************************************************************************************
     ****************************************  SUMA PRECIO  ***************************************
     **********************************************************************************************/
    public void sumaPrecio(double cantidad) {
        precioTotal = precioTotal+cantidad;
        vectorPrecios.add(precioTotal);

        posicionPrecio++;

        textSwitcher.setInAnimation(inSuma);
        textSwitcher.setOutAnimation(outSuma);

        textSwitcher.setText(String.valueOf(vectorPrecios.elementAt(posicionPrecio)));

    }


    /**********************************************************************************************
     ****************************************  RESTA PRECIO  **************************************
     **********************************************************************************************/
    public void restaPrecio(double cantidad)
    {
        precioTotal = precioTotal-cantidad;
        vectorPrecios.add(precioTotal);

        posicionPrecio++;

        textSwitcher.setInAnimation(inResta);
        textSwitcher.setOutAnimation(outResta);

        textSwitcher.setText(String.valueOf(vectorPrecios.elementAt(posicionPrecio)));

    }


    public void restaIngrediente(String ingrediente)
    {
        if (ingrediente.equals("Lechuga")) {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Pepino"))
        {
            restaPrecio(3.0);
        }
        else if (ingrediente.equals("Aceituna"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Kiwi"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Cebolla"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Tomate"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Zanahoria"))
        {
            restaPrecio(3.0);
        }
        else if (ingrediente.equals("Huevo"))
        {
            restaPrecio(3.0);
        }
        else if (ingrediente.equals("Berenjena"))
        {
            restaPrecio(5.0);
        }
        else if (ingrediente.equals("Queso"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Champiñon"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Tocino"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Naranja"))
        {
            restaPrecio(3.0);
        }
        else if (ingrediente.equals("Mora"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Platano"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Frambuesa"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Fresa"))
        {
            restaPrecio(3.0);
        }
        else if (ingrediente.equals("Chile Amarillo"))
        {
            restaPrecio(1.50);
        }
        else if (ingrediente.equals("Chile Rojo"))
        {
            restaPrecio(1.50);
        }
        else if (ingrediente.equals("Chipotle"))
        {
            restaPrecio(5.0);
        }
        else if (ingrediente.equals("Caesar"))
        {
            restaPrecio(5.0);
        }
        else if (ingrediente.equals("Ranch"))
        {
            restaPrecio(5.0);
        }
        else if (ingrediente.equals("Berry"))
        {
            restaPrecio(5.0);
        }
        else if (ingrediente.equals("Honey"))
        {
            restaPrecio(5.0);
        }
    }



    /**********************************************************************************************
     ************************************  Long Click Listener  ***********************************
     **********************************************************************************************/

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            DragShadow dragShadow = new DragShadow(v);

            v.startDrag(null,dragShadow, v, 0);

            return true;
        }
    };




    /**********************************************************************************************
     **************************************  On Drag Listener  ************************************
     **********************************************************************************************/
    View.OnDragListener DropListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            int dragEvent = event.getAction();

            switch (dragEvent)
            {
                case DragEvent.ACTION_DRAG_ENTERED:
                    Log.i("Drag Event", "Entered");
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    Log.i("Drag Event","Exited");
                    break;

                case DragEvent.ACTION_DROP:

                    float x = event.getX();
                    float y = event.getY();
                    Dibujar_y_Almacenar_Ingrediente(x, y);
                    //Toast.makeText(getApplicationContext(),"Ingrediente Agregado",Toast.LENGTH_SHORT).show();



                    break;
            }


            return true;
        }
    };






    /**********************************************************************************************
     **************************************  ANIMACION TEXTO **************************************
     **********************************************************************************************/
    public void init()
    {
        textSwitcher = (TextSwitcher)findViewById(R.id.price);

        inSuma  = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        outSuma = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        inResta  = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        outResta = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
    }


    void setFactory() {
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                TextView myText = new TextView(salad.this);
                myText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                //myText.setTextColor(getResources().getColor(R.color.md_amber_100));
                myText.setTextSize(35);
                return myText;
            }
        });

    }



    /**********************************************************************************************
     **************************************  ANIMACION PLATO **************************************
     **********************************************************************************************/
    @Override
    public void onWindowFocusChanged (boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if(focus){
            ImageView Plato = (ImageView)findViewById(R.id.plato);
            AbsoluteLayout dropArea = (AbsoluteLayout)(findViewById(R.id.dropArea));
            ImageView display = (ImageView)(findViewById(R.id.display));

            //Animacion
            int LI = Plato.getWidth();
            int AI = Plato.getHeight();
            int V1 = LI/2;
            int X = dropArea.getRight();
            int CX = X/2;
            int Y = dropArea.getBottom();
            int posI = Plato.getLeft();
            int posF = CX-V1;
            int distanciaAvanzada = posF-posI;

            int AvanzarX = distanciaAvanzada;
            int AvanzarY = (Y-AI)-60;

            //Plato.setX(AvanzarX);
            Plato.setY(AvanzarY);


            TranslateAnimation animation = new TranslateAnimation(0, AvanzarX, 0, 0); //animacion de bajada
            animation.setDuration(250);
            animation.setFillAfter(true);
            Plato.setAnimation(animation); //Agragar animacion al plato


            //DropArea
            int dropAreaRight = dropArea.getRight();

            //Display
            int bottomDisplay = (display.getBottom());

            //Trashcan
            ImageView trashcan = (ImageView)findViewById(R.id.trashcan);
            int trashcanWidth = trashcan.getWidth();
            int trashcanHeight = trashcan.getHeight();
            int trashcanMiddleH = trashcanHeight/2;
            int trashY = (bottomDisplay - trashcanMiddleH);
            int trashX = (dropAreaRight-(trashcanWidth+trashY));

            trashcan.setX(trashX);
            trashcan.setY(trashY);

            //Display Precio
            price = (TextSwitcher)findViewById(R.id.price);
            int alturaDisplay = display.getHeight();
            int alturaPrecio = price.getHeight();
            int priceY = ((alturaDisplay-alturaPrecio)/2);

            price.setY(priceY);


            focus = false;
        }
    }


    /**********************************************************************************************
     **************************************  Drag Shadow Class  ************************************
     **********************************************************************************************/

    private class DragShadow extends View.DragShadowBuilder
    {

        ImageView imagen;

        public DragShadow(View view) {
            super(view);
            SelectedImage = view.getId(); //Obtener ID de imagen seleccionada
            imagen = (ImageView)findViewById(SelectedImage);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
        {
            View v = getView();

            int height = (int) v.getHeight();
            int width = (int) v.getWidth();
            shadowSize.set(width, height); //Tamaño del Shadow
            shadowTouchPoint.set((int)width,(int)height); //Posicion del Shadow
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            imagen.draw(canvas); //Dibujar el canvas de la imagen
        }
    }








}
