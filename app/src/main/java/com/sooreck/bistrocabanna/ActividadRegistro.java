package com.sooreck.bistrocabanna;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


public class ActividadRegistro extends AppCompatActivity {


    // Progress Dialog
    private ProgressDialog pDialog;

    // JSON  class
    JSONParser jsonParser = new JSONParser();

    private static final String REGISTER_URL = "http://mariocastro.96.lt/basedatos/register.php";

    //ids
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    private TextInputLayout tilNombre;
    private TextInputLayout tilTelefono;
    private TextInputLayout tilUsuario;
    private TextInputLayout tilPassword;
    private TextInputLayout tilConfirmPassword;
    private TextInputLayout tilDireccion;

    private EditText campoNombre;
    private EditText campoTelefono;
    private EditText campoUsuario;
    private EditText campoPassword;
    private EditText campoConfirmPassword;
    private EditText campoDireccion;

    public String url = "http://mariocastro.96.lt/basedatos/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        Toolbar toolbar = (Toolbar) findViewById(com.sooreck.bistrocabanna.R.id.toolbar);
        setSupportActionBar(toolbar);

        // Referencias TILs
        tilNombre = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_nombre);
        tilTelefono = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_telefono);
        tilUsuario = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_usuario);
        tilPassword = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_password);
        tilConfirmPassword = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_confirm_password);
        tilDireccion = (TextInputLayout) findViewById(com.sooreck.bistrocabanna.R.id.til_direccion);

        // Referencias ETs
        campoNombre = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_nombre);
        campoTelefono = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_telefono);
        campoUsuario = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_usuario);
        campoPassword = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_password);
        campoConfirmPassword = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_confirm_password);
        campoDireccion = (EditText) findViewById(com.sooreck.bistrocabanna.R.id.campo_direccion);

        campoNombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilNombre.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        campoTelefono.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilTelefono.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        campoUsuario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilUsuario.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        campoPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (tilPassword.getEditText().getText().toString().length()>=8){
                    tilPassword.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        campoConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilConfirmPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        campoDireccion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilDireccion.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Referencia Botón
        Button botonAceptar = (Button) findViewById(com.sooreck.bistrocabanna.R.id.boton_aceptar);
        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarDatos();
            }
        });

        Button botonCancelar = (Button) findViewById(com.sooreck.bistrocabanna.R.id.boton_cancelar);
        botonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

    }//Termina el onCreate

    private boolean esNombreValido(String nombre) {
        Pattern patron = Pattern.compile("^[a-zA-Z ]+$");
        if (!patron.matcher(nombre).matches() || nombre.length() > 35) {
            tilNombre.setError("Nombre Invalido");
            return false;
        } else {
            tilNombre.setError(null);
        }

        return true;
    }

    private boolean esTelefonoValido(String telefono) {
        if (!Patterns.PHONE.matcher(telefono).matches()) {
            tilTelefono.setError("Telefono Invalido");
            return false;
        } else {
            tilTelefono.setError(null);
        }

        return true;
    }

  /*  private boolean esCorreoValido(String correo) {
        if (!Patterns.EMAIL_ADDRESS.matcher(correo).matches()) {
            tilCorreo.setError("Invalid E-mail");
            return false;
        } else {
            tilCorreo.setError(null);
        }

        return true;
    } */

    private boolean esUsuarioValido(String usuario) {
        Pattern patron = Pattern.compile("^[A-Za-z0-9]+$");
        if (!patron.matcher(usuario).matches() || usuario.length() > 30) {
            tilUsuario.setError("Usuario Invalido");
            return false;
        } else {
            tilUsuario.setError(null);
        }

        return true;
    }

    private boolean esPasswordValido(String password) {
        if (password.length() < 8) {
            tilPassword.setError("Debe ser mayor de 8 digitos");
            return false;
        } else {
            tilPassword.setError(null);
        }

        return true;
    }

    private boolean esConfirmPasswordValido(String confirmPassword,String password) {
        if (confirmPassword.equals(password)) {
            if (confirmPassword.equals("")) {
                tilConfirmPassword.setError("Este campo es necesario");
                return false;
            }

            tilConfirmPassword.setError(null);
            return true;
        } else {
            tilConfirmPassword.setError("Revisa que se tu misma password");
        }

        return false;
    }

    private boolean esDireccionValido(String direccion) {

        if (direccion.length() > 250) {
            tilDireccion.setError("Direccion Invalida");
            return false;
        } else {
            tilDireccion.setError(null);
        }

        return true;
    }

    private void validarDatos() {
        String nombre = tilNombre.getEditText().getText().toString();
        String telefono = tilTelefono.getEditText().getText().toString();
        String usuario = tilUsuario.getEditText().getText().toString();
        String password = tilPassword.getEditText().getText().toString();
        String confirmPassword = tilConfirmPassword.getEditText().getText().toString();
        String direccion = tilDireccion.getEditText().getText().toString();

        boolean a = esNombreValido(nombre);
        boolean b = esTelefonoValido(telefono);
        boolean d = esUsuarioValido(usuario);
        boolean e = esPasswordValido(password);
        boolean f = esConfirmPasswordValido(confirmPassword,password);
        boolean g = esDireccionValido(direccion);

        if (a && b && d && e && f && g) {
            new CreateUser().execute(nombre, telefono, usuario, password, direccion);

        }

    }

    /*Para RED*/

    class CreateUser extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActividadRegistro.this);
            pDialog.setMessage("Registrando usuario en el sistema");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {

            // Check for success tag
            int success;

            String nombre = args[0];
            String telefono =args[1];
            String usuario = args[2];
            String password = args[3];
            String direccion = args[4];
            Log.d("******PRUEBA","nombre "+args[0]+"  pass:"+args[3]);
            try {
                // Building Parameters
                List params = new ArrayList();
                params.add(new BasicNameValuePair("nombre", nombre));
                params.add(new BasicNameValuePair("telefono", telefono));
                params.add(new BasicNameValuePair("username", usuario));
                params.add(new BasicNameValuePair("password", password));
                params.add(new BasicNameValuePair("direccion", direccion));



                Log.d("request!", "starting");

                //Posting user data to script
                JSONObject json = jsonParser.makeHttpRequest(
                        REGISTER_URL, "POST", params);

                // full json response
                Log.d("Registrando intento", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Log.d("Usuario Creado", json.toString());
                    finish();
                    return json.getString(TAG_MESSAGE);
                }else{
                    Log.d("Fallo el registro", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            if (file_url != null){
                Toast.makeText(ActividadRegistro.this, file_url, Toast.LENGTH_LONG).show();
            }
        }
    }

}
