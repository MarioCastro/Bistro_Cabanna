package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 01/04/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mapa;
    MarkerOptions nuevascoordenadas;
    String nuevadireccion;
    LatLng bistroCabannaLocation;
    EditText etnewdirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mapa);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
        mapa = mapFragment.getMap();
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapa.setMyLocationEnabled(true);

        etnewdirection = (EditText) findViewById(R.id.et_nueva_direccion);


        // Obtener instancia de la action bar
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            // Habilitar el Up Button
            actionBar.setDisplayHomeAsUpEnabled(true);
            // Cambiar icono del Up Button
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_close);
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {

        bistroCabannaLocation = new LatLng(24.009232, -104.661077);
        map.addMarker(new MarkerOptions().position(bistroCabannaLocation)
                        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                        .title("Bistro Cabanna")
                        .snippet("Restaurante")
                        .draggable(false)
        );

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(bistroCabannaLocation, 18.0f));


        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(final LatLng point) {
                map.clear();
                map.addMarker(new MarkerOptions().position(bistroCabannaLocation)
                                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                                .title("Bistro Cabanna")
                                .snippet("Restaurante")
                                .draggable(false)
                );

                Projection proj = map.getProjection();
                Point coord = proj.toScreenLocation(point);

                /*Toast.makeText(
                        MapsActivity.this,
                        "Click Largo\n" +
                                "Lat: " + point.latitude + "\n" +
                                "Lng: " + point.longitude + "\n" +
                                "X: " + coord.x + " - Y: " + coord.y,
                        Toast.LENGTH_SHORT).show(); */
                nuevascoordenadas = new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                        .title("Nueva Direccion")
                        .snippet("Tu pedido se enviará estas coordenadas")
                        .draggable(false);
                map.addMarker(nuevascoordenadas);
                etnewdirection.setText("");
                etnewdirection.setText(String.valueOf(point.latitude) + ", " + String.valueOf(point.longitude));

            }
        });

        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                Location myLocation = location;


            }
        });

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {

                    Location location = map.getMyLocation();
                    etnewdirection.setText(String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));

                } catch (NullPointerException ex){
                    Toast.makeText(
                            MapsActivity.this,
                            "Revisa que este activado tu GPS\n" +
                                    "e intenta de nuevo",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dialog_cambiar_direccion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                Intent returnIntentCancel = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntentCancel);
                finish();
                break;
            case R.id.action_save:
                nuevadireccion = etnewdirection.getText().toString();
                Intent returnIntentOK = new Intent();
                returnIntentOK.putExtra("newaddress",nuevadireccion);
                setResult(Activity.RESULT_OK, returnIntentOK);
                finish();

                break;
        }

        return super.onOptionsItemSelected(item);
    }



}