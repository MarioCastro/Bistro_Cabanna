package com.sooreck.bistrocabanna;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.sooreck.bistrocabanna.R;



/**
 * Fragmento para la pestaña "PERFIL" De la sección "Mi Cuenta"
 */
public class FragmentoPerfil extends Fragment {

    private Cliente cliente;

    private TextView nameTextView;
    private TextView phoneTextView;
    private TextView direccionTextView;
    private TextView usuarioTextView;

    private ImageView iconoCambiarPassword;

    public FragmentoPerfil() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragmento_perfil, container, false);

        Intent i = getActivity().getIntent();
        cliente = (Cliente)i.getSerializableExtra("cliente");

        nameTextView = (TextView) view.findViewById(R.id.texto_nombre);
        nameTextView.setText(cliente.getName());

        phoneTextView = (TextView) view.findViewById(R.id.texto_phone);
        phoneTextView.setText(cliente.getPhone());

        direccionTextView = (TextView) view.findViewById(R.id.texto_direccion);
        direccionTextView.setText(cliente.getAddresss());

        usuarioTextView = (TextView) view.findViewById(R.id.texto_usuario);
        usuarioTextView.setText(cliente.getUser());

        iconoCambiarPassword = (ImageView) view.findViewById(R.id.icono_indicador_derecho);

        iconoCambiarPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                new DialogCambiarPassword().show(fm, "CambiaPassword");

            }
        });


        return view;
    }

}/*Termina la clase FragemntoPErfil*/