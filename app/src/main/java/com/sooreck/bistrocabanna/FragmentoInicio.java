package com.sooreck.bistrocabanna;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sooreck.bistrocabanna.R;

/*
 * Fragmento para la sección de "Inicio"
 */


public class FragmentoInicio extends Fragment {
    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorInicio adaptador;
    private Cliente cliente;

    public FragmentoInicio() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_inicio, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(layoutManager);

        adaptador = new AdaptadorInicio(Comida.COMIDAS_POPULARES);

        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("SI","CLICK");
                Intent idatos = getActivity().getIntent();
                cliente = (Cliente) idatos.getSerializableExtra("cliente");

                int posicion = reciclador.getChildAdapterPosition(v);
                //Toast.makeText(getActivity(), "Click en "+posicion, Toast.LENGTH_LONG).show();

                Intent i = new Intent(getActivity(), ActividadAgregarCarrito.class);
                i.putExtra("infoCliente", cliente);
                i.putExtra("platillo", Comida.COMIDAS_POPULARES.get(posicion).getNombre());
                i.putExtra("ingredientes", "Especialidad de la casa");
                i.putExtra("precio", Comida.COMIDAS_POPULARES.get(posicion).getPrecio());
                i.putExtra("imagen", Comida.COMIDAS_POPULARES.get(posicion).getIdDrawable());
                startActivity(i);

            }
        });

        reciclador.setAdapter(adaptador);
        return view;
    }

}