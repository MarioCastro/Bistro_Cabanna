package com.sooreck.bistrocabanna;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 03/03/2016.
 */
public class Pedidos {

    public String platillo;
    public String direccion;
    public String fecha;
    public String estado;
    public String preciototal;

    public Pedidos(String platillo, String direccion,
                   String fecha, String estado, String preciototal) {
        this.platillo = platillo;
        this.direccion = direccion;
        this.fecha = fecha;
        this.estado = estado;
        this.preciototal = preciototal;
    }


    public String getPlatillo() {
        return platillo;
    }

    public void setPlatillo(String platillo) {
        this.platillo = platillo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPreciototal() {
        return preciototal;
    }

    public void setPreciototal(String precio) {
        this.preciototal = precio;
    }

}//Termina la clase
