package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 29/03/2016.
 */
public class ItemCarrito {
    public String platillo;
    public String direccion;
    public String fecha;
    public String estado;
    public String preciototal;
    public String user;
    public String ingredientes;

    public ItemCarrito(String platillo, String direccion,
                   String fecha, String estado, String preciototal, String user, String ingredientes) {
        this.platillo = platillo;
        this.direccion = direccion;
        this.fecha = fecha;
        this.estado = estado;
        this.preciototal = preciototal;
        this.user=user;
        this.ingredientes=ingredientes;

    }


    public String getPlatillo() {
        return platillo;
    }

    public void setPlatillo(String platillo) {
        this.platillo = platillo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPreciototal() {
        return preciototal;
    }

    public void setPreciototal(String precio) {
        this.preciototal = precio;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

}//Termina la clase