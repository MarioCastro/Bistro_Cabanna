package com.sooreck.bistrocabanna;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.util.Vector;

public class Hamburguesa extends AppCompatActivity {

    public int SelectedImage, posicionPrecio = 0;
    public boolean focus = true;
    Animation inSuma, outSuma, inResta, outResta;
    TextSwitcher textSwitcher;
    private ViewGroup marco;
    public TextSwitcher price;
    double precioTotal = 0.0;
    ImageView DrawImage;
    Rect rect;
    Vector <String>  V1 = new Vector <String> ();
    Vector <Integer> V2 = new Vector <Integer> ();
    Vector <Double> vectorPrecios = new Vector <Double> ();

    /**********************************************************************************************
     ******************************************  MÉTODOS ******************************************
     **********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredientes_hamburguesa);

        /****************************** IMAGENES CON LONGCLICKLISTENER *****************************/
        LonkClickListeners();

        /*********************************** DOUBLE TAP LISTENER ************************************/
        DoubleTapListeners();

        /************************************* BOTON TERMINAR **************************************/
        BotonTerminar();

        /********************************* AGREGAR PRECIO INICIAL **********************************/
        vectorPrecios.add(precioTotal);
        init();
        setFactory();

        /****************************************** LAYOUT *****************************************/
        //Identificar el Drop Area
        findViewById(R.id.dropArea).setOnDragListener(DropListener);
        marco = (ViewGroup)findViewById(R.id.dropArea);

    }





    /**********************************************************************************************
     ************************************  Dibujar Hamburguesa  ***********************************
     **********************************************************************************************/

    public void Dibujar_y_Almacenar_Ingrediente(final float x, float y)
    {

        //Se genera una ID aleatoria de 8 digitos para el View seleccinado
        int value; String key = "";
        for (int i = 0; i < 8; i++)
        {
            value = (int)(Math.random() * 9 + 1);
            key += value;
        }
        //Se convierte el String a valor numérico
        int ID = Integer.parseInt(key);

        //Layout
        AbsoluteLayout absoluteLayout = (AbsoluteLayout)findViewById(R.id.dropArea);
        DrawImage = new ImageView(this);  //Imagen que se va a dibujar

        if (R.id.tapa_inferior == SelectedImage) {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.tapa_inferior));
            V1.add("Tapa Inferior");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);

        }
        else if (R.id.carne == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.carne));
            V1.add("Carne Res");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(8.0);

        }
        else if (R.id.soya == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.soya));
            V1.add("Carne Soya");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(4.0);

        }
        else if (R.id.queso == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.queso));
            V1.add("Queso");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);

        }
        else if (R.id.tomate == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.tomate));
            V1.add("Tomate");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);

        }
        else if (R.id.cebolla == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.cebolla));
            V1.add("Cebolla");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.0);

        }
        else if (R.id.lechuga == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.lechuga));
            V1.add("Lechuga");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(1.50);

        }
        else if (R.id.tapa_superior == SelectedImage)
        {
            DrawImage.setImageDrawable(getResources().getDrawable(R.drawable.tapa_superior));
            V1.add("Tapa Superior");
            V2.add(ID);
            DrawImage.setId(ID);
            sumaPrecio(2.0);

        }


        ImageView dimensiones = (ImageView)findViewById(SelectedImage); //Sacar Imagen Seleccionada

        int width = dimensiones.getWidth();   //Ancho Imagen Seleccionada
        int height = dimensiones.getHeight(); //Alto Imagen Seleccionada

        final int Posx = (int) (x-width);  //Pos X  donde se va a dibujar la imagen
        final int Posy = (int) (y-height); //Pos Y  donde se va a dibujar la imagen

        //Parametros para dibujar la imagen
        final AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams(width+20,height+20,Posx,Posy); //(Ancho , Altura , x , y)

        DrawImage.setLayoutParams(layoutParams);//Pasar los parametros a la imagen Seleccionada
        absoluteLayout.addView(DrawImage);     //Añadir un View al Layout con la imagen Seleccionada


        //Llamar método de Arrastrar ingredientes ya agregados
        ArrastrarIngrediente(absoluteLayout, DrawImage);

    }



    public void ArrastrarIngrediente(final AbsoluteLayout absoluteLayout, final ImageView ingrediente)
    {

        final ImageView trashcan = (ImageView)findViewById(R.id.trashcan);

        // /Arrstrar Ingrediente
        ingrediente.setOnTouchListener(new View.OnTouchListener() {

                                           float lastTouchX, lastTouchY; //ultima posicion  de X y Y
                                           float posX, posY; //Posicion de X y Y general
                                           float posAnteriorX, posAnteriorY; //Posicion anterior de X y Y
                                           float posActualX, posActualY;  //Posicion Actual de X y Y

                                           @Override
                                           public boolean onTouch(View v, MotionEvent event) {

                                               switch (event.getAction()) {
                                                   case MotionEvent.ACTION_DOWN: {
                                                       //Al presionar se guarda la ultima posicion
                                                       lastTouchX = event.getX();
                                                       lastTouchY = event.getY();
                                                       break;
                                                   }
                                                   case MotionEvent.ACTION_UP: {

                                                       int RawX = (int)event.getRawX();
                                                       int RawY = (int)event.getRawY();

                                                       if(Elimino(trashcan,RawX,RawY))
                                                       {
                                                           EliminarIngrediente(ingrediente);
                                                       }

                                                       break;
                                                   }

                                                   case MotionEvent.ACTION_MOVE: {
                                                       posActualX = event.getX() - lastTouchX;
                                                       posActualY = event.getY() - lastTouchY;

                                                       posX = posAnteriorX + posActualX;
                                                       posY = posAnteriorY + posActualY;

                                                       if (posX > 0 && posY > 0 && (posX + v.getWidth()) < absoluteLayout.getWidth() && (posY + v.getHeight()) < absoluteLayout.getHeight()) {
                                                           v.setLayoutParams(new AbsoluteLayout.LayoutParams(v.getMeasuredWidth(), v.getMeasuredHeight(), (int) posX, (int) posY));

                                                           posAnteriorX = posX;
                                                           posAnteriorY = posY;

                                                       }

                                                       break;
                                                   }
                                               }
                                               return true;
                                           }
                                       }

        );

    }



    public boolean Elimino(View view, int x, int y){

        rect = new Rect();
        int [] location = new int[2];

        view.getDrawingRect(rect);
        view.getLocationOnScreen(location);
        rect.offset(location[0], location[1]);
        boolean Flag = rect.contains(x, y);

        return Flag;
    }



    public void showAlertDialog(int imageID)
    {

        //Se crea un inflater
        LayoutInflater inflater = getLayoutInflater();

        //Se Enlaza el View con el Layout
        View viewLayout = inflater.inflate(R.layout.dialog, null);

        //Se Asocia el Boton OK
        Button OK = (Button) viewLayout.findViewById(R.id.ok);

        //Se Asocia el ImageView
        ImageView imagen = (ImageView) viewLayout.findViewById(R.id.imagen);

        //Se saca la Imagen con el ID
        imagen.setImageDrawable(getResources().getDrawable(imageID));

        //Se Crea un Builder
        AlertDialog.Builder builderDialog = new AlertDialog.Builder(this, R.style.Transparente_como_tus_Calzones);

        //Se asigna el Layout
        builderDialog.setView(viewLayout);
        builderDialog.setCancelable(false);

        //Se crea al Alert Dialog
        final AlertDialog alertDialog = builderDialog.create();

        //Se muestra
        alertDialog.show();



        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se Cancela
                alertDialog.cancel();
            }
        });

    }



    /*********************************************************************************************
     ******************************************           *****************************************
     ******************************************  EVENTOS  *****************************************
     ******************************************           *****************************************
     **********************************************************************************************/

    /**********************************************************************************************
     ***********************************  Long Click Listeners  ***********************************
     **********************************************************************************************/
    public void LonkClickListeners()
    {
        findViewById(R.id.tapa_inferior).setOnLongClickListener(longClickListener);
        findViewById(R.id.carne).setOnLongClickListener(longClickListener);
        findViewById(R.id.soya).setOnLongClickListener(longClickListener);
        findViewById(R.id.queso).setOnLongClickListener(longClickListener);
        findViewById(R.id.tomate).setOnLongClickListener(longClickListener);
        findViewById(R.id.cebolla).setOnLongClickListener(longClickListener);
        findViewById(R.id.lechuga).setOnLongClickListener(longClickListener);
        findViewById(R.id.tapa_superior).setOnLongClickListener(longClickListener);
    }


    /**********************************************************************************************
     ***********************************  Double Tap Listeners  ***********************************
     **********************************************************************************************/
    public void DoubleTapListeners()
    {

        // ********************************  Tapa Inferior  ******************************** //
        findViewById(R.id.tapa_inferior).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.pan1;
                showAlertDialog(id);
            }
        });

        // **********************************  Carne Res  ********************************** //
        findViewById(R.id.carne).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.meat;
                showAlertDialog(id);
            }
        });

        // **********************************  Carne Soya  ********************************* //
        findViewById(R.id.soya).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.soya1;
                showAlertDialog(id);
            }
        });

        // ************************************  Queso  ************************************ //
        findViewById(R.id.queso).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.queso1;
                showAlertDialog(id);
            }
        });

        // ************************************  Tomate  *********************************** //
        findViewById(R.id.tomate).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.tomate1;
                showAlertDialog(id);
            }
        });

        // ************************************  Cebolla  ********************************** //
        findViewById(R.id.cebolla).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.cebolla1;
                showAlertDialog(id);
            }
        });

        // ************************************  Lechuuga  ********************************* //
        findViewById(R.id.lechuga).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.lechuga1;
                showAlertDialog(id);
            }
        });

        // ********************************  Tapa Superior  ******************************** //
        findViewById(R.id.tapa_superior).setOnTouchListener(new OnDoubleTapListener(this) {
            @Override
            public void onDoubleTap(MotionEvent e) {
                int id = R.drawable.pan1;
                showAlertDialog(id);
            }
        });
    }


    /**********************************************************************************************
     ********************************  Eliminar Ingrediente Vector  *******************************
     **********************************************************************************************/
    public void EliminarIngrediente(View v)
    {
        int tam = V2.size();
        int ide = v.getId();

        for (int i = 0; i < tam; i++)
        {
            if (V2.elementAt(i)==(ide))
            {
                restaIngrediente(V1.elementAt(i));
                V1.remove(i);
                V2.remove(i);
                v.setVisibility(View.INVISIBLE);
                i = tam + 1;
            }
        }

    }


    /**********************************************************************************************
     *********************************  Mostrar Ingrediente Vector  *******************************
     **********************************************************************************************/
    public String ImprimirIngredientes()
    {
        int tam = V1.size();
        String nuevo;
        String ingredientes = "";

        for (int i = 0; i < tam; i++)
        {
            nuevo = V1.elementAt(i) + ", ";
            ingredientes += nuevo;
        }

        return ingredientes;
    }


    /**********************************************************************************************
     ***************************************  Boton Terminar  *************************************
     **********************************************************************************************/
    public void BotonTerminar()
    {
        final Button finish = (Button)findViewById(R.id.terminar);

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (V1.size() == 0){
                    Toast.makeText(getApplicationContext(),"No has colocado ningun ingrediente",Toast.LENGTH_SHORT).show();
                } else
                if (V1.size() > 0 && V1.size() < 3){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Hamburguesa.this);
                    alertDialogBuilder.setTitle("Bistro Cabanna");
                    alertDialogBuilder.setIcon(R.drawable.ic_doulbecheck);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Muy Pocos Ingredientes, ¿Desea Continuar?")
                            .setCancelable(false)
                            .setPositiveButton("SI",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {

                                    String ingredients = ImprimirIngredientes();

                                    Intent idatos = getIntent();
                                    Cliente cliente = (Cliente)idatos.getSerializableExtra("infoCliente");

                                    Intent i = new Intent(Hamburguesa.this, ActividadAgregarCarrito.class);
                                    i.putExtra("infoCliente", cliente);
                                    i.putExtra("platillo",idatos.getStringExtra("platillo"));
                                    i.putExtra("ingredientes", ingredients);
                                    i.putExtra("precio", String.valueOf(precioTotal));
                                    i.putExtra("imagen", idatos.getIntExtra("imagen",0));
                                    startActivityForResult(i, 100);

                                }
                            })
                            .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {

                    String ingredients = ImprimirIngredientes();
                    Toast.makeText(getApplicationContext(),ingredients,Toast.LENGTH_SHORT).show();
                    Intent idatos = getIntent();
                    Cliente cliente = (Cliente)idatos.getSerializableExtra("infoCliente");

                    Intent i = new Intent(Hamburguesa.this, ActividadAgregarCarrito.class);
                    i.putExtra("infoCliente", cliente);
                    i.putExtra("platillo",idatos.getStringExtra("platillo"));
                    i.putExtra("ingredientes", ingredients);
                    i.putExtra("precio", String.valueOf(precioTotal));
                    i.putExtra("imagen", idatos.getIntExtra("imagen",0));
                    startActivityForResult(i, 100);
                }

            }
        });
    }



    /**********************************************************************************************
     ****************************************  SUMA PRECIO  ***************************************
     **********************************************************************************************/
    public void sumaPrecio(double cantidad) {
        precioTotal = precioTotal+cantidad;
        vectorPrecios.add(precioTotal);

        posicionPrecio++;

        textSwitcher.setInAnimation(inSuma);
        textSwitcher.setOutAnimation(outSuma);

        textSwitcher.setText(String.valueOf(vectorPrecios.elementAt(posicionPrecio)));

    }


    /**********************************************************************************************
     ****************************************  RESTA PRECIO  **************************************
     **********************************************************************************************/
    public void restaPrecio(double cantidad)
    {
        precioTotal = precioTotal-cantidad;
        vectorPrecios.add(precioTotal);

        posicionPrecio++;

        textSwitcher.setInAnimation(inResta);
        textSwitcher.setOutAnimation(outResta);

        textSwitcher.setText(String.valueOf(vectorPrecios.elementAt(posicionPrecio)));

    }


    public void restaIngrediente(String ingrediente)
    {
        if (ingrediente.equals("Tapa Inferior"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Carne Res"))
        {
            restaPrecio(8.0);
        }
        else if (ingrediente.equals("Carne Soya"))
        {
            restaPrecio(4.0);
        }
        else if (ingrediente.equals("Queso"))
        {
            restaPrecio(2.0);
        }
        else if (ingrediente.equals("Tomate"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Cebolla"))
        {
            restaPrecio(1.0);
        }
        else if (ingrediente.equals("Lechuga"))
        {
            restaPrecio(1.50);
        }
        else if (ingrediente.equals("Tapa Superior"))
        {
            restaPrecio(2.0);
        }
    }



    /**********************************************************************************************
     ************************************  Long Click Listener  ***********************************
     **********************************************************************************************/

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            DragShadow dragShadow = new DragShadow(v);

            /*
            SelectedImage = v.getId();
            ImageView imagen = (ImageView)findViewById(SelectedImage);
            View.DragShadowBuilder builder = new View.DragShadowBuilder(imagen);
            */

            v.startDrag(null,dragShadow, v, 0);

            return true;
        }
    };




    /**********************************************************************************************
     **************************************  On Drag Listener  ************************************
     **********************************************************************************************/
    View.OnDragListener DropListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            int dragEvent = event.getAction();

            switch (dragEvent)
            {
                case DragEvent.ACTION_DRAG_ENTERED:
                    Log.i("Drag Event", "Entered");
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    Log.i("Drag Event","Exited");
                    break;

                case DragEvent.ACTION_DROP:

                    float x = event.getX();
                    float y = event.getY();
                    Dibujar_y_Almacenar_Ingrediente(x, y);
                    //Toast.makeText(getApplicationContext(),"Ingrediente Agregado",Toast.LENGTH_SHORT).show();



                    break;
            }


            return true;
        }
    };






    /**********************************************************************************************
     **************************************  ANIMACION TEXTO **************************************
     **********************************************************************************************/
    public void init()
    {
        textSwitcher = (TextSwitcher)findViewById(R.id.price);

        inSuma  = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        outSuma = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        inResta  = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        outResta = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
    }


    void setFactory() {
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                TextView myText = new TextView(Hamburguesa.this);
                myText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                myText.setTextColor(getResources().getColor(R.color.md_blue_grey_800));
                myText.setTextSize(35);
                return myText;
            }
        });

    }



    /**********************************************************************************************
     **************************************  ANIMACION PLATO **************************************
     **********************************************************************************************/
    @Override
    public void onWindowFocusChanged (boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if(focus){
            ImageView Plato = (ImageView)findViewById(R.id.plato);
            //Animacion
            //Para acomodar imagen hasta el fondo del Layout
            int bottom = findViewById(R.id.dropArea).getBottom(); //Sacar Fondo del Layout
            int alturaPlato = (Plato.getHeight());
            int FinalBottom = (bottom-alturaPlato)+275;
            int AnchoImagen = Plato.getWidth()/2; //Sacar anchura de imagen
            int xlenght = (int)findViewById(R.id.dropArea).getWidth()/2;  //Sacar la mitad de lo ancho del Layout
            int Centro = (int)(findViewById(R.id.dropArea).getLeft()+xlenght)-AnchoImagen; //Sacar centro del Layout

            Plato.setX(Centro); //Posicionar imagen en el centro

            TranslateAnimation animation = new TranslateAnimation(0, 0, 0, FinalBottom); //animacion de bajada
            animation.setDuration(300);
            animation.setFillAfter(true);
            findViewById(R.id.plato).setAnimation(animation); //Agragar animacion al plato


            //DropArea
            AbsoluteLayout dropArea = (AbsoluteLayout)findViewById(R.id.dropArea);
            int dropAreaRight = dropArea.getRight();

            //Display
            ImageView display = (ImageView)findViewById(R.id.display);
            int bottomDisplay = (display.getBottom());

            //Trashcan
            ImageView trashcan = (ImageView)findViewById(R.id.trashcan);
            int trashcanWidth = trashcan.getWidth();
            int trashcanHeight = trashcan.getHeight();
            int trashcanMiddleH = trashcanHeight/2;
            int trashY = (bottomDisplay - trashcanMiddleH);
            int trashX = (dropAreaRight-(trashcanWidth+trashY));

            trashcan.setX(trashX);
            trashcan.setY(trashY);

            //Display Precio
            price = (TextSwitcher)findViewById(R.id.price);
            int alturaDisplay = display.getHeight();
            int alturaPrecio = price.getHeight();
            int priceY = ((alturaDisplay-alturaPrecio)/2);

            price.setY(priceY);


            focus = false;
        }
    }


    /**********************************************************************************************
     **************************************  Drag Shadow Class  ************************************
     **********************************************************************************************/

    private class DragShadow extends View.DragShadowBuilder
    {

        ImageView imagen;

        public DragShadow(View view) {
            super(view);
            SelectedImage = view.getId(); //Obtener ID de imagen seleccionada
            imagen = (ImageView)findViewById(SelectedImage);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
        {
            View v = getView();

            int height = (int) v.getHeight();
            int width = (int) v.getWidth();
            shadowSize.set(width, height); //Tamaño del Shadow
            shadowTouchPoint.set((int)width,(int)height); //Posicion del Shadow
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            imagen.draw(canvas); //Dibujar el canvas de la imagen
        }
    }

    /**********************************************************************************************
     ***********************************  Double Touch Listener  ***********************************
     **********************************************************************************************/

    public class OnDoubleTapListener implements View.OnTouchListener {

        private GestureDetector gestureDetector;

        public OnDoubleTapListener(Context c) {
            gestureDetector = new GestureDetector(c, new GestureListener());
        }

        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            return gestureDetector.onTouchEvent(motionEvent);
        }


        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                OnDoubleTapListener.this.onDoubleTap(e);
                return super.onDoubleTap(e);
            }
        }

        public void onDoubleTap(MotionEvent e) {
            // To be overridden when implementing listener
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100){
            if (resultCode == Activity.RESULT_OK) {
                Log.e("SI","Si lo mando");
                finish();
            }
        }


    }
}




/*
*
*       PRECIOS
*
*       TAPAS (Superior e Inferior) ---> $2.00
*       CARNE RES X1   ----------------> $8.00
*       CARNE SOYA X1  ----------------> $4.00
*       QUESO X1   --------------------> $2.00
*       TOMATE X2 ---------------------> $1.00
*       CEBOLLA X3 --------------------> $1.00
*       LECHUGA HOJA (X1) -------------> $1.50
*
*
* */
