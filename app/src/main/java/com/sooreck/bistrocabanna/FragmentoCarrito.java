package com.sooreck.bistrocabanna;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;


import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 30/03/2016.
 */
public class FragmentoCarrito extends Fragment {

    private LinearLayoutManager linearLayout;
    public static RecyclerView reciclador;
    public static AdaptadorItemCarrito adaptador;

    private TextView total;
    private Button vaciar;
    private Button terminar;
    private RadioGroup groupTipoPago;
    private RadioButton radioEfectivo;
    private RadioButton radioPayPal;


    public static List<ItemCarrito> items;

    Cliente cliente;

    private ProgressDialog pDialog;
    // JSON  class
    JSONParser jsonParser = new JSONParser();

    private static final String URL = "http://mariocastro.96.lt/basedatos/delete_pedidos.php";
    //ids
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String CONFIG_CLIENT_ID = "IDAcb4OdOwgUDvRlJGxiMxsKgOVnoEn3I9_jqqA7UdnkvESwTleMvaom4NHyIVGCsILcs90L19VHv9X2Uq";
    /*Live*/
    //private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    //private static final String CONFIG_CLIENT_ID = "ARoSDblDK15eTD3MysaTjvFHPf8bZRq89_xqULPAFNhT-w9852lOPCCUgrdC0FElI2klgQJ3DoVczDs-";

    public static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .merchantName("Bistro Cabanna")
            .merchantPrivacyPolicyUri(Uri.parse("http://www.BistroCabanna.com/Privacy"))
            .merchantUserAgreementUri(Uri.parse("http://www.BistroCabanna.com/"))
            .clientId(CONFIG_CLIENT_ID);

    public FragmentoCarrito() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_carrito, container, false );

        reciclador = (RecyclerView)view.findViewById(R.id.reciclador_micuenta_carrito);
        linearLayout = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayout);

        total = (TextView) view.findViewById(R.id.precio_total_carrito);
        vaciar = (Button) view.findViewById(R.id.boton_vaciar);
        terminar = (Button) view.findViewById(R.id.boton_terminar);
        groupTipoPago = (RadioGroup) view.findViewById(R.id.grupo_tipo_pago);
        groupTipoPago.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == radioEfectivo.getId()){
                    terminar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final SharedPreferences preferences = getActivity().getSharedPreferences("CarritoUser", Context.MODE_PRIVATE);
                            if (! (preferences.getFloat("total",0f) == 0.0) ){
                                new UpdatePedidos().execute(cliente.getUser(), "En Cola");
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                total.setText("$ 0.0");
                                items = new ArrayList<>();
                                adaptador = new AdaptadorItemCarrito(items);
                                reciclador.setAdapter(adaptador);
                                radioPayPal.setChecked(false);
                                radioEfectivo.setChecked(false);
                            }



                        }
                    });
                }
                if (checkedId == radioPayPal.getId()){
                    terminar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final SharedPreferences preferences = getActivity().getSharedPreferences("CarritoUser", Context.MODE_PRIVATE);
                            if (! (preferences.getFloat("total",0f) == 0.0) ){
                                onBuyPressed();
                                Toast.makeText(getActivity(), "tu forma de pago es PayPal", Toast.LENGTH_LONG).show();
                            }

                            /*SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            total.setText("$ 0.0");
                            items = new ArrayList<>();
                            adaptador = new AdaptadorItemCarrito(items);
                            reciclador.setAdapter(adaptador);
                            radioPayPal.setChecked(false);
                            radioEfectivo.setChecked(false); */
                        }
                    });
                }

            }
        });
        radioEfectivo = (RadioButton) view.findViewById(R.id.radioButtonEfectivo);
        radioPayPal = (RadioButton) view.findViewById(R.id.radioButtonCredito);

        Intent i = getActivity().getIntent();
        cliente = (Cliente)i.getSerializableExtra("cliente");

        final SharedPreferences preferences = getActivity().getSharedPreferences("CarritoUser", Context.MODE_PRIVATE);
        Gson gson = new Gson();

                /*Se obtiene el carrito del usuario*/
        String saveList = preferences.getString("carrito", "");

        if (saveList.equals("")){
            items = new ArrayList<>();
        } else {
                   /*Conseguir el tipo de dato para que gson sepa a que tipo de dato convetir*/
            Type type = new TypeToken<List<ItemCarrito>>(){}.getType();
                /* Convertimos el string en el list*/
            items = gson.fromJson(saveList,type);
        }

        total.setText("$ "+String.valueOf(preferences.getFloat("total",0f)));

        adaptador = new AdaptadorItemCarrito(items);
        reciclador.setAdapter(adaptador);

        vaciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                total.setText("$ 0.0");
                items = new ArrayList<>();
                adaptador = new AdaptadorItemCarrito(items);
                reciclador.setAdapter(adaptador);
                radioPayPal.setChecked(false);
                radioEfectivo.setChecked(false);
                new VaciarCarrito().execute(cliente.getUser());
            }
        });
        terminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Selecciona una forma de pago", Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }// Termina onCreateView

    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        getActivity().startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        PayPalItem[] itemsPaypal = new PayPalItem[items.size()];
        for (int i=0; i< items.size(); i++){

            itemsPaypal[i] = new PayPalItem(items.get(i).getPlatillo(), 1,new BigDecimal(items.get(i).getPreciototal()),
                    "MXN",items.get(i).getPlatillo());
        }

        BigDecimal subtotal = PayPalItem.getItemTotal(itemsPaypal);
        BigDecimal shipping = new BigDecimal("0.00");
        BigDecimal tax = new BigDecimal("0.00");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "MXN", "Tu compra", paymentIntent);
        payment.items(itemsPaypal).paymentDetails(paymentDetails);
        return payment;
    }

    /***********************************************************************************************
     ************************  CLASE PARA VACIAR CARRITO DE LA B.D. ********************************
     **********************************************************************************************/
    class VaciarCarrito extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {

            // Check for success tag
            int success;

            String username = args[0];

            try {
                // Building Parameters
                List params = new ArrayList();
                params.add(new BasicNameValuePair("username", username));
                Log.d("request!", "starting");

                //Posting user data to script
                JSONObject json = jsonParser.makeHttpRequest(
                        URL, "POST", params);

                // full json response
                Log.d("Registrando intento", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Log.d("Carrito eliminado", json.toString());
                    return json.getString(TAG_MESSAGE);
                }else{
                    Log.d("Fallo al eliminar", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            if (file_url != null){
                Toast.makeText(getActivity(), file_url, Toast.LENGTH_LONG).show();
            }
        }
    }// Termina la clase para vaciar el carrito de la base de datos


public void vaciaelCarritoExtend(){
    SharedPreferences preferences = getActivity().getSharedPreferences("CarritoUser", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.clear();
    editor.apply();
    total.setText("$ 0.0");
    items = new ArrayList<>();
    adaptador = new AdaptadorItemCarrito(items);
    reciclador.setAdapter(adaptador);
    radioPayPal.setChecked(false);
    radioEfectivo.setChecked(false);
    new VaciarCarrito().execute(cliente.getUser());
}

}//Termina clase FragmentoCarrito
