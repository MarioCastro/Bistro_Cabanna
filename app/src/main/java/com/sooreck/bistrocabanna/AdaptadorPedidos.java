package com.sooreck.bistrocabanna;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SoOreck on 03/03/2016.
 */
public class AdaptadorPedidos extends RecyclerView.Adapter<AdaptadorPedidos.ViewHolder> {

    private List<Pedidos> items;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView platillo;
        public TextView direccion;
        public TextView fecha;
        public TextView estado;
        public TextView precio;



        public ViewHolder(View v) {
            super(v);
            platillo = (TextView) v.findViewById(R.id.texto_platillo);
            direccion = (TextView) v.findViewById(R.id.texto_dir);
            fecha = (TextView) v.findViewById(R.id.texto_fecha);
            estado = (TextView) v.findViewById(R.id.texto_estado);
            precio = (TextView) v.findViewById(R.id.texto_precio);
        }
    }

    public AdaptadorPedidos(List<Pedidos> items) {
        this.items = items;
    }

    @Override
    public AdaptadorPedidos.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_pedidos, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorPedidos.ViewHolder holder, int position) {
        holder.platillo.setText("Platillo: "+items.get(position).getPlatillo());
        holder.direccion.setText("Direccion: "+items.get(position).getDireccion());
        holder.fecha.setText("Fecha: "+items.get(position).getFecha());
        holder.estado.setText("Estado: "+items.get(position).getEstado());
        holder.precio.setText("Total: $"+items.get(position).getPreciototal());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
