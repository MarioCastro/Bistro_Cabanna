package com.sooreck.bistrocabanna;

/**
 * Created by SoOreck on 23/02/2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.sooreck.bistrocabanna.R;

import org.json.JSONException;

public class ActividadUsuarioPrincipal extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    public static Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_usuario);

        agregarToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            ponerDrawer(navigationView);
            // Seleccionar item por defecto
            seleccionarItem(navigationView.getMenu().getItem(0));
        }

        Intent i = getIntent();
        cliente = (Cliente)i.getSerializableExtra("cliente");

        View headerLayout = navigationView != null ? navigationView.getHeaderView(0) : null;
        TextView userHeader = (TextView) (headerLayout != null ? headerLayout.findViewById(R.id.user_header) : null);
        if (userHeader != null) {
            userHeader.setText(cliente.getName());
        }

    }//Fin del onCrate

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }


    private void ponerDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

    }//Fin del metodo ponerDrawer

    private void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemDrawer.getItemId()) {
            case R.id.item_inicio:
                fragmentoGenerico = new FragmentoInicio();
                break;
            case R.id.item_cuenta:
                // Fragmento para la sección Cuenta
                fragmentoGenerico = new FragmentoCuenta();
                break;
            case R.id.item_menu:
                fragmentoGenerico = new FragmentoMenu();
                break;
            case R.id.item_quejas_sugerencias:
                // Iniciar actividad de configuración
                fragmentoGenerico = new FragmentoQuejasSugerencias();
                break;
            case R.id.item_salir:
                SharedPreferences sp = getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sp.edit();
                edit.putBoolean("sesionGuardada",false);
                edit.apply();

                Intent intent = new Intent(ActividadUsuarioPrincipal.this, Login.class);
                startActivity(intent);
                finish();
                break;
            case R.id.item_configuracion:
                fragmentoGenerico = new FragmentoConfiguracion();
                break;
        }
        if (fragmentoGenerico != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, fragmentoGenerico) /*  se reemplaza el contenido del RelativeLayout identificado por R.id.contenedor_principal que esta en actividad_principal.xml  */
                    .commit();
        }

        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actividad_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_cerrar_sesion:
                SharedPreferences sp = getSharedPreferences("BistroCabannaData", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sp.edit();
                edit.putBoolean("sesionGuardada",false);
                edit.apply();

                Intent intent = new Intent(ActividadUsuarioPrincipal.this, Login.class);
                startActivity(intent);
                finish();

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FragmentoCarrito.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e("Show", confirm.toJSONObject().toString(4));
                        Log.e("Show", confirm.getPayment().toJSONObject().toString(4));
                        Toast.makeText(getApplicationContext(), "PaymentConfirmation received", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "an extremely unlikely failure" +
                                " occurred:", Toast.LENGTH_LONG).show();
                    }
                }
                new UpdatePedidos().execute(cliente.getUser(), "En Cola");

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Se Canceló el pago", Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(getApplicationContext(), "Hubo un error al realizar el pago", Toast.LENGTH_LONG).show();
            }
        }
    }




}